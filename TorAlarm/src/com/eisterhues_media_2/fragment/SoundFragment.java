package com.eisterhues_media_2.fragment;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import butterknife.InjectView;

import com.eisterhues_media_2.R;
import com.eisterhues_media_2.SettingsFragmentActivity;
import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.utils.Utils;

public class SoundFragment extends BaseFragment {

	@InjectView(R.id.check_mute)
	CheckBox mMute;

	@InjectView(R.id.check_vib)
	CheckBox mVib;

	@InjectView(R.id.check_default)
	CheckBox mDefault;

	@InjectView(R.id.check_whistle)
	CheckBox mWhistle;

	@InjectView(R.id.check_honk)
	CheckBox mHonk;

	@InjectView(R.id.check_jingle)
	CheckBox mJingle;

	@InjectView(R.id.check_cheer)
	CheckBox mCheer;

	@Override
	public int layout() {
		return R.layout.fragment_sound;
	}

	public SoundFragment() {
	}

	@Override
	public void onCreate() {
		
		if (((SettingsFragmentActivity) getParentActivity()).getRight() == null) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.sound));

			((BaseActivity) getParentActivity()).getBackButton().setVisibility(
					View.VISIBLE);
			((BaseActivity) getParentActivity()).getBackButton()
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View view) {
							((SettingsFragmentActivity) getParentActivity()).settings();
						}
					});
			
			((BaseActivity) getParentActivity()).getMenuButton().setVisibility(View.GONE);
		}

		if (((BaseActivity) getParentActivity()).getPrefsManager()
				.isPushMuteEnable())
			mMute.setChecked(true);
		mMute.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				((BaseActivity) getParentActivity()).getPrefsManager()
						.setPushMuteEnable(isChecked);
			}
		});

		if (((BaseActivity) getParentActivity()).getPrefsManager()
				.isPushVibEnable())
			mVib.setChecked(true);
		mVib.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				((BaseActivity) getParentActivity()).getPrefsManager()
						.setPushVibEnable(isChecked);
			}
		});

		if (((BaseActivity) getParentActivity()).getPrefsManager()
				.getNotyficationSound() == 0)
			mDefault.setChecked(true);
		mDefault.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					((BaseActivity) getParentActivity()).getPrefsManager()
							.setNotyficationSound(0);
					mHonk.setChecked(false);
					mWhistle.setChecked(false);
					mJingle.setChecked(false);
					mCheer.setChecked(false);

					Utils.play(getParentActivity(), 0);
				}
			}
		});

		if (((BaseActivity) getParentActivity()).getPrefsManager()
				.getNotyficationSound() == 1)
			mWhistle.setChecked(true);
		mWhistle.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					((BaseActivity) getParentActivity()).getPrefsManager()
							.setNotyficationSound(1);
					mDefault.setChecked(false);
					mHonk.setChecked(false);
					mJingle.setChecked(false);
					mCheer.setChecked(false);

					Utils.play(getParentActivity(), 1);
				}
			}
		});

		if (((BaseActivity) getParentActivity()).getPrefsManager()
				.getNotyficationSound() == 2)
			mHonk.setChecked(true);
		mHonk.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					((BaseActivity) getParentActivity()).getPrefsManager()
							.setNotyficationSound(2);
					mDefault.setChecked(false);
					mWhistle.setChecked(false);
					mJingle.setChecked(false);
					mCheer.setChecked(false);

					Utils.play(getParentActivity(), 2);
				}
			}
		});

		if (((BaseActivity) getParentActivity()).getPrefsManager()
				.getNotyficationSound() == 3)
			mJingle.setChecked(true);
		mJingle.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					((BaseActivity) getParentActivity()).getPrefsManager()
							.setNotyficationSound(3);
					mDefault.setChecked(false);
					mHonk.setChecked(false);
					mWhistle.setChecked(false);
					mCheer.setChecked(false);

					Utils.play(getParentActivity(), 3);
				}
			}
		});

		if (((BaseActivity) getParentActivity()).getPrefsManager()
				.getNotyficationSound() == 4)
			mCheer.setChecked(true);
		mCheer.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					((BaseActivity) getParentActivity()).getPrefsManager()
							.setNotyficationSound(4);
					mDefault.setChecked(false);
					mHonk.setChecked(false);
					mWhistle.setChecked(false);
					mJingle.setChecked(false);

					Utils.play(getParentActivity(), 4);
				}
			}
		});
	}
}
