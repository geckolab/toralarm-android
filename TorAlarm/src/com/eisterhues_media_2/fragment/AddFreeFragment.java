package com.eisterhues_media_2.fragment;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.eisterhues_media_2.MainActivity;
import com.eisterhues_media_2.R;
import com.eisterhues_media_2.SettingsFragmentActivity;
import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.utils.Const;

public class AddFreeFragment extends BaseFragment {

	@Override
	public int layout() {
		return R.layout.fragment_add_free;
	}

	public AddFreeFragment() {
	}

	@Override
	public void onCreate() {

		if (((SettingsFragmentActivity) getParentActivity()).getRight() == null) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.add_free));

			((BaseActivity) getParentActivity()).getBackButton().setVisibility(
					View.VISIBLE);
			((BaseActivity) getParentActivity()).getBackButton()
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View view) {
							if (!((SettingsFragmentActivity) getParentActivity())
									.getIntent().getExtras()
									.getBoolean(Const.ADD_REQUEST, false)) {
								((SettingsFragmentActivity) getParentActivity())
										.settings();
							} else {
								Intent i = new Intent(getParentActivity(),
										MainActivity.class);
								startActivity(i);
							}
						}
					});
			((BaseActivity) getParentActivity()).getMenuButton().setVisibility(
					View.GONE);
		}

	}

}
