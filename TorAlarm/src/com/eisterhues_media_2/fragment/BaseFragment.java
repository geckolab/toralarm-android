
package com.eisterhues_media_2.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;

@SuppressLint("NewApi")
public class BaseFragment extends Fragment {

	private Activity mActivity;
	private View mView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mActivity = super.getActivity();
		mView = inflater.inflate(layout(), container, false);
		ButterKnife.inject(this, mView);

		onCreate();

		return mView;
	}

	public int layout() {
		return 0;
	}

	public void onCreate() {
	}

	public Activity getParentActivity() {
		return mActivity;
	}

	public void setParentActivity(Activity activity) {
		mActivity = activity;
	}

}
