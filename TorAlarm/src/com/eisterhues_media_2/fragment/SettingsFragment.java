package com.eisterhues_media_2.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import butterknife.InjectView;

import com.eisterhues_media_2.MainActivity;
import com.eisterhues_media_2.R;
import com.eisterhues_media_2.SettingsFragmentActivity;
import com.eisterhues_media_2.SupportActivity;
import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.utils.Const;

@SuppressLint({ "NewApi", "ValidFragment" })
public class SettingsFragment extends BaseFragment {

	@InjectView(R.id.check_notyfications)
	CheckBox mNotyfications;

	@InjectView(R.id.push_de)
	RelativeLayout mPushDE;

	@InjectView(R.id.check_liga1)
	CheckBox mLiga1;

	@InjectView(R.id.check_liga2)
	CheckBox mLiga2;

	@InjectView(R.id.check_liga3)
	CheckBox mLiga3;

	@InjectView(R.id.check_pokal)
	CheckBox mPokal;

	@InjectView(R.id.check_el)
	CheckBox mEL;

	@InjectView(R.id.check_cl)
	CheckBox mCL;

	@InjectView(R.id.sound)
	RelativeLayout mSound;

	@InjectView(R.id.support)
	RelativeLayout mSupport;

	@InjectView(R.id.purchase)
	RelativeLayout mPurchase;

	@Override
	public int layout() {
		return R.layout.fragment_settings;
	}

	public SettingsFragment() {

	}

	@Override
	public void onCreate() {
		if (((SettingsFragmentActivity) getParentActivity()).getRight() == null) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.settings));

			((BaseActivity) getParentActivity()).getMenuButton().setVisibility(
					View.VISIBLE);
			((BaseActivity) getParentActivity()).getMenuButton()
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View view) {
							Intent i = new Intent(getParentActivity(),
									MainActivity.class);
							startActivity(i);
						}
					});

			((BaseActivity) getParentActivity()).getBackButton().setVisibility(
					View.GONE);
		}

		mNotyfications.setChecked(((BaseActivity) getParentActivity())
				.getPrefsManager().isPushNotificationEnable());
		mNotyfications
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						((BaseActivity) getParentActivity()).getPrefsManager()
								.setPushNotificationEnable(isChecked);
					}
				});

		mLiga1.setChecked(((BaseActivity) getParentActivity())
				.getPrefsManager().isLiga1Enable());
		mLiga1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				((BaseActivity) getParentActivity()).getPrefsManager()
						.setLiga1Enable(isChecked);
			}
		});

		mLiga2.setChecked(((BaseActivity) getParentActivity())
				.getPrefsManager().isLiga2Enable());
		mLiga2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				((BaseActivity) getParentActivity()).getPrefsManager()
						.setLiga2Enable(isChecked);
			}
		});

		mLiga3.setChecked(((BaseActivity) getParentActivity())
				.getPrefsManager().isLiga3Enable());
		mLiga3.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				((BaseActivity) getParentActivity()).getPrefsManager()
						.setLiga3Enable(isChecked);
			}
		});

		mPokal.setChecked(((BaseActivity) getParentActivity())
				.getPrefsManager().isPokalEnable());
		mPokal.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				((BaseActivity) getParentActivity()).getPrefsManager()
						.setPokalEnable(isChecked);
			}
		});

		mEL.setChecked(((BaseActivity) getParentActivity()).getPrefsManager()
				.isELEnable());
		mEL.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				((BaseActivity) getParentActivity()).getPrefsManager()
						.setELEnable(isChecked);
			}
		});

		mCL.setChecked(((BaseActivity) getParentActivity()).getPrefsManager()
				.isCLEnable());
		mCL.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				((BaseActivity) getParentActivity()).getPrefsManager()
						.setCLEnable(isChecked);
			}
		});

		mPushDE.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((SettingsFragmentActivity) getParentActivity())
						.push(Const.PUSH_DE);
			}
		});

		mSound.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((SettingsFragmentActivity) getParentActivity()).sound();
			}
		});

		mSupport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getParentActivity(),
						SupportActivity.class);
				startActivity(i);
			}
		});

		mPurchase.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((SettingsFragmentActivity) getParentActivity()).ad();
			}
		});

	}

}
