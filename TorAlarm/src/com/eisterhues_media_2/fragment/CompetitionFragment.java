package com.eisterhues_media_2.fragment;

import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;

import com.eisterhues_media_2.CompetitionFragmentActivity;
import com.eisterhues_media_2.DetailsActivity;
import com.eisterhues_media_2.MainActivity;
import com.eisterhues_media_2.R;
import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.network.NetworkHelper;
import com.eisterhues_media_2.network.NetworkHelperInterface;
import com.eisterhues_media_2.network.input.InputParams;
import com.eisterhues_media_2.network.response.Matches;
import com.eisterhues_media_2.network.response.Matches.Data;
import com.eisterhues_media_2.network.response.Matches.Round;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.NetworkUtils;
import com.eisterhues_media_2.utils.Utils;
import com.squareup.picasso.Picasso;

@SuppressLint({ "NewApi", "ValidFragment" })
public class CompetitionFragment extends BaseFragment {

	private Handler handler = new Handler();
	private Runnable action = null;

	private NetworkHelper mNetworkHelper;
	private LayoutInflater mInflater;
	private View mRoundHeaderView;
	private View mRoundDetailsView;
	private int mSelectedRound;
	private int mCompetitionID;
	private String mCompetition;

	private Spinner mSpinner;

	@InjectView(R.id.linearLayoutMatches)
	LinearLayout mMatches;

	@Optional
	@InjectView(R.id.spinner)
	Spinner mSpinnerLayout;

	@Optional
	@InjectView(R.id.bar)
	RelativeLayout mBar;

	@Optional
	@InjectView(R.id.swipe_container)
	SwipeRefreshLayout mRefresh;

	@Optional
	@OnClick(R.id.table)
	public void send(View view) {
		((CompetitionFragmentActivity) getParentActivity()).table();
	}

	@Override
	public int layout() {
		return R.layout.fragment_competition;
	}

	public CompetitionFragment() {

	}

	public CompetitionFragment(String competition, int selectedRound) {
		mCompetition = competition;
		mSelectedRound = selectedRound;
	}

	@Override
	public void onCreate() {

		if (((BaseActivity) getParentActivity()).getSpinner() != null)
			mSpinner = ((BaseActivity) getParentActivity()).getSpinner();
		else
			mSpinner = mSpinnerLayout;

		if (((CompetitionFragmentActivity) getParentActivity()).getRight() == null) {
			mRefresh.setOnRefreshListener(new OnRefreshListener() {

				@Override
				public void onRefresh() {

					mRefresh.setOnRefreshListener(this);
					mRefresh.setColorScheme(android.R.color.white,
							android.R.color.holo_green_light,
							android.R.color.white,
							android.R.color.holo_green_light);

					getData(true);
				}

			});
		}

		mInflater = LayoutInflater.from(getParentActivity());

		if (mCompetition.equals(Const.LIGA1)) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.liga1));
			mCompetitionID = 0;
			mSpinner.setVisibility(View.VISIBLE);
		} else if (mCompetition.equals(Const.LIGA2)) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.liga2));
			mCompetitionID = 0;
			mSpinner.setVisibility(View.VISIBLE);
		} else if (mCompetition.equals(Const.LIGA3)) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.liga3));
			mCompetitionID = 0;
			mSpinner.setVisibility(View.VISIBLE);
		} else if (mCompetition.equals(Const.CL)) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.cl));
			mCompetitionID = 5;
			mSpinner.setVisibility(View.VISIBLE);
		} else if (mCompetition.equals(Const.EL)) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.el));
			mCompetitionID = 5;
			mSpinner.setVisibility(View.VISIBLE);
		} else if (mCompetition.equals(Const.POKAL)) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.pokal));
			mCompetitionID = 3;
			mSpinner.setVisibility(View.GONE);

			if (mBar != null)
				mBar.setVisibility(View.GONE);
		} else if (mCompetition.equals(Const.LAND)) {
			((BaseActivity) getParentActivity()).getTitleView().setText(
					getString(R.string.land));
			mCompetitionID = 4;
			mSpinner.setVisibility(View.GONE);

			if (mBar != null)
				mBar.setVisibility(View.GONE);
		}
		if (((BaseActivity) getParentActivity()).getSpinner() == null) {

			((BaseActivity) getParentActivity()).getBackButton().setVisibility(
					View.GONE);

			((BaseActivity) getParentActivity()).getMenuButton().setVisibility(
					View.VISIBLE);
			((BaseActivity) getParentActivity()).getMenuButton()
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View view) {
							Intent i = new Intent(getParentActivity(),
									MainActivity.class);
							startActivity(i);
						}
					});

			((BaseActivity) getParentActivity()).getReloadButton()
					.setVisibility(View.VISIBLE);
			((BaseActivity) getParentActivity()).getReloadButton()
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View view) {
							getData(true);
						}
					});
		}

		getData(false);

		action = new Runnable() {
			@Override
			public void run() {
				getData(false);
				handler.postDelayed(action, 30000);
			}
		};
		handler.postDelayed(action, 30000);

	}

	@Override
	public void onDestroy() {
		handler.removeCallbacks(action);
		super.onDestroy();
	}

	private void initSpinner(List<String> list, int currentRound, int maxRound) {
		ArrayAdapter<String> dataAdapter = new RoundSpinnerAdapter(
				getParentActivity(), R.layout.simple_spinner_item, list,
				currentRound, maxRound);
		dataAdapter
				.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
		mSpinner.setAdapter(dataAdapter);
		mSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				if (pos != mSelectedRound - 1) {
					mSelectedRound = pos + 1;
					getData(false);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

	}

	public void getData(final boolean refresh) {
		if (!NetworkUtils.isNetworkAvailable(getParentActivity()) && refresh)
			Toast.makeText(getParentActivity(),
					getString(R.string.datanotloaded), Toast.LENGTH_SHORT)
					.show();

		mNetworkHelper = new NetworkHelper();
		mNetworkHelper.sentApiRequest(getParentActivity(),
				new NetworkHelperInterface() {

					@Override
					public List<BasicNameValuePair> setApiInputParams() {
						return InputParams.matches(mCompetitionID,
								((BaseActivity) getParentActivity())
										.getLanguage(), mSelectedRound, 0);
					}

					@Override
					public void apiResultAction(String result) {
						try {
							Matches matches = new Matches(
									new JSONObject(result));

							initSpinner(matches.getRounds(),
									matches.getCurrentRound(),
									matches.getSelectableRound());
							if (mSelectedRound == -1)
								mSpinner.setSelection(matches.getCurrentRound() - 1);
							else
								mSpinner.setSelection(mSelectedRound - 1);

							mMatches.removeAllViews();

							for (Round r : matches.getRound()) {
								mRoundHeaderView = mInflater.inflate(
										R.layout.round_header, null);

								TextView left = (TextView) mRoundHeaderView
										.findViewById(R.id.text_left);
								left.setText(Utils.getDate(r.getTime()));

								TextView right = (TextView) mRoundHeaderView
										.findViewById(R.id.text_right);
								if (mSelectedRound == -1)
									right.setText(matches.getRounds().get(
											matches.getCurrentRound() - 1));
								else
									right.setText(matches.getRounds().get(
											mSelectedRound - 1));

								mMatches.addView(mRoundHeaderView);

								for (Data d : r.getData()) {
									mRoundDetailsView = mInflater.inflate(
											R.layout.round_details, null);

									TextView team1 = (TextView) mRoundDetailsView
											.findViewById(R.id.text_left);
									team1.setText(d.getTeam1());

									TextView score = (TextView) mRoundDetailsView
											.findViewById(R.id.text_score);
									score.setText(d.getTeam1Goals() + ":"
											+ d.getTeam2Goals());

									TextView team2 = (TextView) mRoundDetailsView
											.findViewById(R.id.text_right);
									team2.setText(d.getTeam2());

									ImageView team1Icon = (ImageView) mRoundDetailsView
											.findViewById(R.id.team1_icon);
									Picasso.with(getParentActivity())
											.load(Utils.getIconURL(d.getTeam1()))
											.into(team1Icon);

									ImageView team2Icon = (ImageView) mRoundDetailsView
											.findViewById(R.id.team2_icon);
									Picasso.with(getParentActivity())
											.load(Utils.getIconURL(d.getTeam2()))
											.into(team2Icon);

									final Data dd = d;
									mRoundDetailsView
											.setOnClickListener(new OnClickListener() {

												@Override
												public void onClick(View v) {
													if (((CompetitionFragmentActivity) getParentActivity())
															.getRight() == null) {
														Intent i = new Intent(
																getParentActivity(),
																DetailsActivity.class);
														i.putExtra(
																Const.COMPETITION_TYPE,
																mCompetition);
														i.putExtra(
																Const.MATCH_ID,
																dd.getMatchID());
														i.putExtra(
																Const.SELECTED_ROUND,
																mSelectedRound);
														startActivity(i);
													} else {
														Intent i = new Intent(
																getParentActivity(),
																DetailsActivity.class);
														i.putExtra(
																Const.COMPETITION_TYPE,
																mCompetition);
														i.putExtra(
																Const.MATCH_ID,
																dd.getMatchID());
														i.putExtra(
																Const.SELECTED_ROUND,
																mSelectedRound);
														startActivity(i);
													}
												}
											});

									mMatches.addView(mRoundDetailsView);

								}
							}
							if (refresh) {
								Toast.makeText(getParentActivity(),
										getString(R.string.dataloaded),
										Toast.LENGTH_SHORT).show();

								if (((CompetitionFragmentActivity) getParentActivity())
										.getRight() == null)
									mRefresh.setRefreshing(false);
							}
						} catch (JSONException e) {
						}

					}
				});
	}

	public class RoundSpinnerAdapter extends ArrayAdapter<String> {

		private List<String> list;
		private int currentRound;
		private int maxRound;

		public RoundSpinnerAdapter(Context context, int textViewResourceId,
				List<String> list, int currentRound, int maxRound) {
			super(context, textViewResourceId, list);
			this.list = list;
			this.currentRound = currentRound;
			this.maxRound = maxRound;
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = getParentActivity().getLayoutInflater();
			View row = inflater.inflate(R.layout.simple_spinner_dropdown_item,
					parent, false);
			CheckedTextView label = (CheckedTextView) row
					.findViewById(R.id.text1);
			label.setText(list.get(position).toString());

			if (position == currentRound - 1)
				label.setCheckMarkDrawable(R.drawable.icon_green);

			if (position >= maxRound) {
				label.setCheckMarkDrawable(R.drawable.blank);
				label.setClickable(false);
			}

			return row;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getParentActivity().getLayoutInflater();
			View row = inflater.inflate(R.layout.simple_spinner_item, parent,
					false);
			CheckedTextView label = (CheckedTextView) row
					.findViewById(R.id.text1);
			label.setText(list.get(position).toString());

			return row;
		}

	}
}
