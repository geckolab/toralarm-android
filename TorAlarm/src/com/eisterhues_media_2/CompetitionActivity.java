package com.eisterhues_media_2;

import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.InjectView;
import butterknife.OnClick;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.network.NetworkHelper;
import com.eisterhues_media_2.network.NetworkHelperInterface;
import com.eisterhues_media_2.network.input.InputParams;
import com.eisterhues_media_2.network.response.Matches;
import com.eisterhues_media_2.network.response.Matches.Data;
import com.eisterhues_media_2.network.response.Matches.Round;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.NetworkUtils;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;
import com.squareup.picasso.Picasso;

public class CompetitionActivity extends BaseActivity {

	private Handler handler = new Handler();
	private Runnable action = null;

	private NetworkHelper mNetworkHelper;
	private LayoutInflater mInflater;
	private View mRoundHeaderView;
	private View mRoundDetailsView;
	private int mSelectedRound;
	private int mCompetitionID;

	private String mCompetition;

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@InjectView(R.id.linearLayoutMatches)
	LinearLayout mMatches;

	@InjectView(R.id.spinner)
	Spinner mSpinner;

	@InjectView(R.id.bar)
	RelativeLayout mBar;

	@InjectView(R.id.swipe_container)
	SwipeRefreshLayout mRefresh;

	@Override
	public int layout() {
		return R.layout.activity_competition;
	}

	@OnClick(R.id.table)
	public void send(View view) {
		Intent i = new Intent(CompetitionActivity.this, TableActivity.class);
		i.putExtra(Const.COMPETITION_TYPE, mCompetition);
		i.putExtra(Const.SELECTED_ROUND, mSelectedRound);
		startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mRefresh.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {

				mRefresh.setOnRefreshListener(this);
				mRefresh.setColorScheme(android.R.color.white,
						android.R.color.holo_green_light,
						android.R.color.white, android.R.color.holo_green_light);

				getData(true);
			}

		});

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		mInflater = LayoutInflater.from(this);

		mCompetition = getIntent().getExtras()
				.getString(Const.COMPETITION_TYPE);
		mSelectedRound = getIntent().getExtras().getInt(Const.SELECTED_ROUND,
				-1);

		if (mCompetition.equals(Const.LIGA1)) {
			getTitleView().setText(getString(R.string.liga1));
			mCompetitionID = 0;
		} else if (mCompetition.equals(Const.LIGA2)) {
			getTitleView().setText(getString(R.string.liga2));
			mCompetitionID = 0;
		} else if (mCompetition.equals(Const.LIGA3)) {
			getTitleView().setText(getString(R.string.liga3));
			mCompetitionID = 0;
		} else if (mCompetition.equals(Const.CL)) {
			getTitleView().setText(getString(R.string.cl));
			mCompetitionID = 5;
		} else if (mCompetition.equals(Const.EL)) {
			getTitleView().setText(getString(R.string.el));
			mCompetitionID = 5;
		} else if (mCompetition.equals(Const.POKAL)) {
			getTitleView().setText(getString(R.string.pokal));
			mCompetitionID = 3;
			mBar.setVisibility(View.GONE);
		} else if (mCompetition.equals(Const.LAND)) {
			getTitleView().setText(getString(R.string.land));
			mCompetitionID = 4;
			mBar.setVisibility(View.GONE);
		}

		getMenuButton().setVisibility(View.VISIBLE);
		getMenuButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(CompetitionActivity.this,
						MainActivity.class);
				startActivity(i);
			}
		});

		getReloadButton().setVisibility(View.VISIBLE);
		getReloadButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				getData(true);
			}
		});

		getData(false);

		action = new Runnable() {
			@Override
			public void run() {
				getData(false);
				handler.postDelayed(action, 30000);
			}
		};
		handler.postDelayed(action, 30000);

	}

	@Override
	protected void onStop() {
		handler.removeCallbacks(action);
		super.onStart();
	}

	private void initSpinner(List<String> list, int currentRound, int maxRound) {
		ArrayAdapter<String> dataAdapter = new RoundSpinnerAdapter(this,
				R.layout.simple_spinner_item, list, currentRound, maxRound);
		dataAdapter
				.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
		mSpinner.setAdapter(dataAdapter);
		mSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				if (pos != mSelectedRound - 1) {
					mSelectedRound = pos + 1;
					getData(false);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

	}

	private void getData(final boolean refresh) {
		if (!NetworkUtils.isNetworkAvailable(this) && refresh)
			Toast.makeText(getApplicationContext(),
					getString(R.string.datanotloaded), Toast.LENGTH_SHORT)
					.show();

		mNetworkHelper = new NetworkHelper();
		mNetworkHelper.sentApiRequest(this, new NetworkHelperInterface() {

			@Override
			public List<BasicNameValuePair> setApiInputParams() {
				return InputParams.matches(mCompetitionID, getLanguage(),
						mSelectedRound, 0);
			}

			@Override
			public void apiResultAction(String result) {
				try {
					Matches matches = new Matches(new JSONObject(result));

					initSpinner(matches.getRounds(), matches.getCurrentRound(),
							matches.getSelectableRound());
					if (mSelectedRound == -1)
						mSpinner.setSelection(matches.getCurrentRound() - 1);
					else
						mSpinner.setSelection(mSelectedRound - 1);

					mMatches.removeAllViews();

					for (Round r : matches.getRound()) {
						mRoundHeaderView = mInflater.inflate(
								R.layout.round_header, null);

						TextView left = (TextView) mRoundHeaderView
								.findViewById(R.id.text_left);
						left.setText(Utils.getDate(r.getTime()));

						TextView right = (TextView) mRoundHeaderView
								.findViewById(R.id.text_right);
						if (mSelectedRound == -1)
							right.setText(matches.getRounds().get(
									matches.getCurrentRound() - 1));
						else
							right.setText(matches.getRounds().get(
									mSelectedRound - 1));

						mMatches.addView(mRoundHeaderView);

						for (Data d : r.getData()) {
							mRoundDetailsView = mInflater.inflate(
									R.layout.round_details, null);

							TextView team1 = (TextView) mRoundDetailsView
									.findViewById(R.id.text_left);
							team1.setText(d.getTeam1());

							TextView score = (TextView) mRoundDetailsView
									.findViewById(R.id.text_score);
							score.setText(d.getTeam1Goals() + ":"
									+ d.getTeam2Goals());

							TextView team2 = (TextView) mRoundDetailsView
									.findViewById(R.id.text_right);
							team2.setText(d.getTeam2());

							ImageView team1Icon = (ImageView) mRoundDetailsView
									.findViewById(R.id.team1_icon);
							Picasso.with(CompetitionActivity.this)
									.load(Utils.getIconURL(d.getTeam1()))
									.into(team1Icon);

							ImageView team2Icon = (ImageView) mRoundDetailsView
									.findViewById(R.id.team2_icon);
							Picasso.with(CompetitionActivity.this)
									.load(Utils.getIconURL(d.getTeam2()))
									.into(team2Icon);

							final Data dd = d;
							mRoundDetailsView
									.setOnClickListener(new OnClickListener() {

										@Override
										public void onClick(View v) {
											Intent i = new Intent(
													CompetitionActivity.this,
													DetailsActivity.class);
											i.putExtra(Const.COMPETITION_TYPE,
													mCompetition);
											i.putExtra(Const.MATCH_ID,
													dd.getMatchID());
											startActivity(i);
										}
									});

							mMatches.addView(mRoundDetailsView);

						}
					}
					if (refresh) {
						Toast.makeText(getApplicationContext(),
								getString(R.string.dataloaded),
								Toast.LENGTH_SHORT).show();
						mRefresh.setRefreshing(false);
					}
				} catch (JSONException e) {
				}

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}

	public class RoundSpinnerAdapter extends ArrayAdapter<String> {

		private List<String> list;
		private int currentRound;
		private int maxRound;

		public RoundSpinnerAdapter(Context context, int textViewResourceId,
				List<String> list, int currentRound, int maxRound) {
			super(context, textViewResourceId, list);
			this.list = list;
			this.currentRound = currentRound;
			this.maxRound = maxRound;
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.simple_spinner_dropdown_item,
					parent, false);
			CheckedTextView label = (CheckedTextView) row
					.findViewById(R.id.text1);
			label.setText(list.get(position).toString());

			if (position == currentRound - 1)
				label.setCheckMarkDrawable(R.drawable.icon_green);

			if (position >= maxRound) {
				label.setCheckMarkDrawable(R.drawable.blank);
				label.setClickable(false);
			}

			return row;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.simple_spinner_item, parent,
					false);
			CheckedTextView label = (CheckedTextView) row
					.findViewById(R.id.text1);
			label.setText(list.get(position).toString());

			return row;
		}

	}
}
