
package com.eisterhues_media_2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import butterknife.InjectView;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;

public class SettingsActivity extends BaseActivity {

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@InjectView(R.id.check_notyfications)
	CheckBox mNotyfications;

	@InjectView(R.id.push_de)
	RelativeLayout mPushDE;

	@InjectView(R.id.check_liga1)
	CheckBox mLiga1;

	@InjectView(R.id.check_liga2)
	CheckBox mLiga2;

	@InjectView(R.id.check_liga3)
	CheckBox mLiga3;

	@InjectView(R.id.check_pokal)
	CheckBox mPokal;

	@InjectView(R.id.check_el)
	CheckBox mEL;

	@InjectView(R.id.check_cl)
	CheckBox mCL;
	
	@InjectView(R.id.sound)
	RelativeLayout mSound;

	@InjectView(R.id.support)
	RelativeLayout mSupport;

	@InjectView(R.id.purchase)
	RelativeLayout mPurchase;

	@Override
	public int layout() {
		return R.layout.activity_settings;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getTitleView().setText(getString(R.string.settings));

		getMenuButton().setVisibility(View.VISIBLE);
		getMenuButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(SettingsActivity.this, MainActivity.class);
				startActivity(i);
			}
		});

		getParagraphButton().setVisibility(View.VISIBLE);

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		mNotyfications.setChecked(getPrefsManager()
				.isPushNotificationEnable());
		mNotyfications
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						getPrefsManager().setPushNotificationEnable(isChecked);
					}
				});

		mLiga1.setChecked(getPrefsManager()
				.isLiga1Enable());
		mLiga1
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						getPrefsManager().setLiga1Enable(isChecked);
					}
				});

		mLiga2.setChecked(getPrefsManager()
				.isLiga2Enable());
		mLiga2
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						getPrefsManager().setLiga2Enable(isChecked);
					}
				});

		mLiga3.setChecked(getPrefsManager()
				.isLiga3Enable());
		mLiga3
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						getPrefsManager().setLiga3Enable(isChecked);
					}
				});

		mPokal.setChecked(getPrefsManager()
				.isPokalEnable());
		mPokal
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						getPrefsManager().setPokalEnable(isChecked);
					}
				});

		mEL.setChecked(getPrefsManager()
				.isELEnable());
		mEL
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						getPrefsManager().setELEnable(isChecked);
					}
				});

		mCL.setChecked(getPrefsManager()
				.isCLEnable());
		mCL
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						getPrefsManager().setCLEnable(isChecked);
					}
				});

		mPushDE.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(SettingsActivity.this, PushActivity.class);
				i.putExtra(Const.PUSH_ID, Const.PUSH_DE);
				startActivity(i);
			}
		});
		
		mSound.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(SettingsActivity.this, SoundActivity.class);
				startActivity(i);
			}
		});

		mSupport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(SettingsActivity.this, SupportActivity.class);
				startActivity(i);
			}
		});

		mPurchase.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(SettingsActivity.this, AddFreeActivity.class);
				startActivity(i);
			}
		});

	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}

}
