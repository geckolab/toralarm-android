
package com.eisterhues_media_2;

import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.InjectView;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.network.NetworkHelper;
import com.eisterhues_media_2.network.NetworkHelperInterface;
import com.eisterhues_media_2.network.SettingsNetworkHelper;
import com.eisterhues_media_2.network.SettingsNetworkHelper.SettingsNetworkHelperInterface;
import com.eisterhues_media_2.network.input.InputParams;
import com.eisterhues_media_2.network.response.AddToken;
import com.eisterhues_media_2.network.response.GetPushgroup;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.LogUtils;
import com.eisterhues_media_2.utils.NetworkUtils;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;
import com.squareup.picasso.Picasso;

public class PushActivity extends BaseActivity {

	private NetworkHelper mNetworkHelper;
	private SettingsNetworkHelper mSettingsNetworkHelper;
	private LayoutInflater mInflater;
	private View mPushHeaderView;
	private View mPushDetailsView;
	private int mPushID;

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@InjectView(R.id.linearLayoutMatches)
	LinearLayout mMatches;

	@Override
	public int layout() {
		return R.layout.activity_push;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mPushID = getIntent().getExtras().getInt(Const.PUSH_ID);

		getTitleView().setText(getString(R.string.settings));

		getBackButton().setVisibility(View.VISIBLE);
		getBackButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				sendSettings();
				Intent i = new Intent(PushActivity.this, SettingsActivity.class);
				startActivity(i);
			}
		});

		getParagraphButton().setVisibility(View.VISIBLE);

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		mInflater = LayoutInflater.from(this);

		getData();

	}

	private void getData() {
		mNetworkHelper = new NetworkHelper();
		mNetworkHelper.sentApiRequest(this, new NetworkHelperInterface() {

			@Override
			public List<BasicNameValuePair> setApiInputParams() {
				return InputParams.getPushgroup(mPushID, getLanguage());
			}

			@Override
			public void apiResultAction(String result) {
				try {
					GetPushgroup pushes = new GetPushgroup(new JSONObject(result));

					mMatches.removeAllViews();

					mPushHeaderView = mInflater.inflate(R.layout.round_header, null);

					TextView left = (TextView) mPushHeaderView.findViewById(R.id.text_left);
					left.setText(getString(R.string.liga1));

					mMatches.addView(mPushHeaderView);

					getPrefsManager().setPush(mPushID + "-push", 1);

					int i = 1;
					for (String key : pushes.getLiga1().keySet()) {
						mPushDetailsView = mInflater.inflate(R.layout.push_details, null);

						TextView name = (TextView) mPushDetailsView.findViewById(R.id.name);
						name.setText(key);

						ImageView icon = (ImageView) mPushDetailsView
								.findViewById(R.id.icon);
						Picasso.with(PushActivity.this)
								.load(Utils.getIconURL(key))
								.into(icon);

						final int ii = i;

						if (!getPrefsManager().getPrefs().contains(mPushID + "-" + ii))
							getPrefsManager().setPush(mPushID + "-" + ii, 1);

						CheckBox check = (CheckBox) mPushDetailsView
								.findViewById(R.id.check);

						check.setChecked(getPrefsManager()
								.getPush(mPushID + "-" + ii, 1) == 1 ? true : false);
						check
								.setOnCheckedChangeListener(new OnCheckedChangeListener() {
									public void onCheckedChanged(CompoundButton buttonView,
											boolean isChecked) {
										getPrefsManager().setPush(mPushID + "-" + ii,
												isChecked ? 1 : 0);
									}
								});

						mMatches.addView(mPushDetailsView);
						i++;
					}

					mPushHeaderView = mInflater.inflate(R.layout.round_header, null);

					left = (TextView) mPushHeaderView.findViewById(R.id.text_left);
					left.setText(getString(R.string.liga2));

					mMatches.addView(mPushHeaderView);

					for (String key : pushes.getLiga2().keySet()) {
						mPushDetailsView = mInflater.inflate(R.layout.push_details, null);

						TextView name = (TextView) mPushDetailsView.findViewById(R.id.name);
						name.setText(key);

						ImageView icon = (ImageView) mPushDetailsView
								.findViewById(R.id.icon);
						Picasso.with(PushActivity.this)
								.load(Utils.getIconURL(key))
								.into(icon);

						final int ii = i;

						if (!getPrefsManager().getPrefs().contains(mPushID + "-" + ii))
							getPrefsManager().setPush(mPushID + "-" + ii, 1);

						CheckBox check = (CheckBox) mPushDetailsView
								.findViewById(R.id.check);

						check.setChecked(getPrefsManager()
								.getPush(mPushID + "-" + ii, 1) == 1 ? true : false);
						check
								.setOnCheckedChangeListener(new OnCheckedChangeListener() {
									public void onCheckedChanged(CompoundButton buttonView,
											boolean isChecked) {
										getPrefsManager().setPush(mPushID + "-" + ii,
												isChecked ? 1 : 0);
									}
								});

						mMatches.addView(mPushDetailsView);
						i++;
					}

					mPushHeaderView = mInflater.inflate(R.layout.round_header, null);

					left = (TextView) mPushHeaderView.findViewById(R.id.text_left);
					left.setText(getString(R.string.liga3));

					mMatches.addView(mPushHeaderView);

					for (String key : pushes.getLiga3().keySet()) {
						mPushDetailsView = mInflater.inflate(R.layout.push_details, null);

						TextView name = (TextView) mPushDetailsView.findViewById(R.id.name);
						name.setText(key);

						ImageView icon = (ImageView) mPushDetailsView
								.findViewById(R.id.icon);
						Picasso.with(PushActivity.this)
								.load(Utils.getIconURL(key))
								.into(icon);

						final int ii = i;

						if (!getPrefsManager().getPrefs().contains(mPushID + "-" + ii))
							getPrefsManager().setPush(mPushID + "-" + ii, 0);

						CheckBox check = (CheckBox) mPushDetailsView
								.findViewById(R.id.check);

						check.setChecked(getPrefsManager()
								.getPush(mPushID + "-" + ii, 0) == 1 ? true : false);
						check
								.setOnCheckedChangeListener(new OnCheckedChangeListener() {
									public void onCheckedChanged(CompoundButton buttonView,
											boolean isChecked) {
										getPrefsManager().setPush(mPushID + "-" + ii,
												isChecked ? 1 : 0);
									}
								});

						mMatches.addView(mPushDetailsView);
						i++;
					}

					mPushHeaderView = mInflater.inflate(R.layout.round_header, null);

					left = (TextView) mPushHeaderView.findViewById(R.id.text_left);
					left.setText(getString(R.string.land));

					mMatches.addView(mPushHeaderView);

					for (String key : pushes.getInternational().keySet()) {
						mPushDetailsView = mInflater.inflate(R.layout.push_details, null);

						TextView name = (TextView) mPushDetailsView.findViewById(R.id.name);
						name.setText(key);

						ImageView icon = (ImageView) mPushDetailsView
								.findViewById(R.id.icon);
						Picasso.with(PushActivity.this)
								.load(Utils.getIconURL(key))
								.into(icon);

						final int ii = i;

						if (!getPrefsManager().getPrefs().contains(mPushID + "-" + ii))
							getPrefsManager().setPush(mPushID + "-" + ii, 1);

						CheckBox check = (CheckBox) mPushDetailsView
								.findViewById(R.id.check);

						check.setChecked(getPrefsManager()
								.getPush(mPushID + "-" + ii, 1) == 1 ? true : false);
						check
								.setOnCheckedChangeListener(new OnCheckedChangeListener() {
									public void onCheckedChanged(CompoundButton buttonView,
											boolean isChecked) {
										getPrefsManager().setPush(mPushID + "-" + ii,
												isChecked ? 1 : 0);
									}
								});

						mMatches.addView(mPushDetailsView);
						i++;
					}

				} catch (JSONException e) {
				}
			}
		});
	}

	private void sendSettings() {
		if (!NetworkUtils.isNetworkAvailable(this))
			Toast.makeText(getApplicationContext(),
					getString(R.string.pushnotsaved),
					Toast.LENGTH_SHORT).show();

		JSONObject settings = new JSONObject();

		if (!getPrefsManager().getPrefs().contains("1-push")) {
			try {
				LogUtils.d("MODE", "DEF");
				settings.put("0", new JSONArray(Const.PUSH_DE_DEFAULT));
			} catch (JSONException e) {
			}
		} else {
			String arr = "[";
			for (int i = 1; i <= Const.PUSH_DE_NUMBER; i++) {
				arr += getPrefsManager().getPush("1-" + i, 1);

				if (i < Const.PUSH_DE_NUMBER)
					arr += ",";
			}
			arr += "]";

			try {
				LogUtils.d("MODE", "USER");
				settings.put("0", new JSONArray(arr));
			} catch (JSONException e) {
			}
		}

		mSettingsNetworkHelper = new SettingsNetworkHelper();
		mSettingsNetworkHelper.sentApiRequest(this, InputParams.settings(getPrefsManager()
				.getDeviceID(),
				settings, Const.DEVICE_TYPE, getLanguage(), String.valueOf(getPrefsManager()
						.getAppVersion()), String.valueOf(android.os.Build.VERSION.SDK_INT),
				android.os.Build.MODEL,
				Utils.isTablet(this), getPrefsManager().getLastAction(), getPrefsManager()
						.getAppStarts()), new SettingsNetworkHelperInterface() {

			@Override
			public void apiResultAction(JSONArray result) {
				try {
					AddToken out = new AddToken(result);
				} catch (JSONException e) {
				}

				Toast.makeText(getApplicationContext(),
						getString(R.string.pushsaved),
						Toast.LENGTH_SHORT).show();
			}
		});

	}
	
	@Override
	public void onBackPressed() {
		sendSettings();
		super.onBackPressed();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}

}
