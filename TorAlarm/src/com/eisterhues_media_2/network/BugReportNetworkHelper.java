
package com.eisterhues_media_2.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.LogUtils;
import com.eisterhues_media_2.utils.NetworkUtils;

public class BugReportNetworkHelper {

	public void sentApiRequest(Context context,
			JSONObject params, BugNetworkHelperInterface bugNetworkHelperInterface) {
		if (NetworkUtils.isNetworkAvailable(context)) {
			try {
				new ApiRequestTask(params, bugNetworkHelperInterface).execute();
			} catch (Exception e) {
				LogUtils.e("NetworError", e.toString());
			}
		}
	}

	public class ApiRequestTask extends AsyncTask<Void, Void, Void> {

		private JSONObject mParams;
		private BugNetworkHelperInterface mSettingsNetworkHelperInterface;

		public ApiRequestTask(JSONObject params,
				BugNetworkHelperInterface settingsNetworkHelperInterface) {
			mParams = params;
			mSettingsNetworkHelperInterface = settingsNetworkHelperInterface;
		}

		@Override
		protected Void doInBackground(Void... params) {
			InputStream inputStream = null;
			JSONObject jObj = null;

			try {
				DefaultHttpClient httpClient = new DefaultHttpClient();

				String url = Const.API_BUG_REPORT_URL;
				LogUtils.d("URL", url);

				HttpPost httpPost = new HttpPost(url);

				StringEntity entity = new StringEntity(mParams.toString(), HTTP.UTF_8);
				LogUtils.d("INPUT", mParams.toString());
				entity.setContentType("application/json");

				httpPost.setEntity(entity);

				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream, "utf-8"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				inputStream.close();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				LogUtils.e("Buffer Error",
						"Error converting result " + e.toString());
			}
			
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			mSettingsNetworkHelperInterface.apiResultAction();
			super.onPostExecute(result);
		}
	}

	public interface BugNetworkHelperInterface {
		public void apiResultAction();
	}

}
