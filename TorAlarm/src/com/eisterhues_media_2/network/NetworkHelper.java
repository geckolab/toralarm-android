
package com.eisterhues_media_2.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.os.AsyncTask;

import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.LogUtils;
import com.eisterhues_media_2.utils.NetworkUtils;
import com.eisterhues_media_2.utils.Utils;

public class NetworkHelper {

	public void sentApiRequest(Context context,
			NetworkHelperInterface networkHelperInterface) {
		if (NetworkUtils.isNetworkAvailable(context)) {
			try {
				new ApiRequestTask(networkHelperInterface).execute();
			} catch (Exception e) {
				LogUtils.e("NetworError", e.toString());
			}
		}
	}

	public class ApiRequestTask extends AsyncTask<Void, Void, String> {

		private NetworkHelperInterface mNetworkHelperInterface;

		public ApiRequestTask(NetworkHelperInterface networkHelperInterface) {
			mNetworkHelperInterface = networkHelperInterface;
		}

		@Override
		protected String doInBackground(Void... params) {

			InputStream inputStream = null;
			String jObj = null;
			String url = Const.API_URL + "?"
					+ URLEncodedUtils.format(mNetworkHelperInterface.setApiInputParams(), "utf-8");

			if (Const.ALLOW_DEBUG)
				url += "&decode";

			LogUtils.d("URL", url);

			try {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpGet httpGet = new HttpGet(url);

				HttpResponse httpResponse = httpClient.execute(httpGet);
				HttpEntity httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream, "utf-8"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
					LogUtils.d("RESPONSE", line);
				}
				inputStream.close();

				if (Const.ALLOW_DEBUG)
					jObj = sb.toString();
				else
					jObj = Utils.decode(sb.toString());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				LogUtils.e("Buffer Error",
						"Error converting result " + e.toString());
			}

			return jObj;
		}

		@Override
		protected void onPostExecute(String result) {
			mNetworkHelperInterface.apiResultAction(result);
			super.onPostExecute(result);
		}
	}

}
