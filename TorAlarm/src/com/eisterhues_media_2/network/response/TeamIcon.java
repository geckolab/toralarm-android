
package com.eisterhues_media_2.network.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

public class TeamIcon {

	private List<String> mIcons = new ArrayList<String>();

	public TeamIcon(JSONArray data) throws JSONException {
		for (int i = 0, size = data.length(); i < size; i++)
			mIcons.add(data.getString(i));
	}

	public List<String> getIcons() {
		return mIcons;
	}

	public void setIcons(List<String> icons) {
		mIcons = icons;
	}

}
