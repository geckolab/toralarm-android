
package com.eisterhues_media_2.network.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AddToken {

	private List<Action> mActions = new ArrayList<Action>();

	public AddToken(JSONArray data) throws JSONException {
		JSONObject action = null;

		int actionID = 0;
		int actionType = 0;
		String title = "";
		String text = "";
		String okTitle = "";
		String cancelTitle = "";
		String url = "";

		for (int i = 0, size = data.length(); i < size; i++) {
			action = data.getJSONObject(i);

			if (action.has("action_id"))
				actionID = action.getInt("action_id");

			if (action.has("action_type"))
				actionType = action.getInt("action_type");

			if (action.has("title"))
				title = action.getString("title");

			if (action.has("text"))
				text = action.getString("text");

			if (action.has("ok_title"))
				okTitle = action.getString("ok_title");

			if (action.has("cancel_title"))
				cancelTitle = action.getString("cancel_title");

			if (action.has("url"))
				url = action.getString("url");

			mActions.add(new Action(actionID, actionType, title, text, okTitle, cancelTitle, url));

			actionID = 0;
			actionType = 0;
			title = "";
			text = "";
			okTitle = "";
			cancelTitle = "";
			url = "";
		}
	}

	public class Action {
		private int mActionID;
		private int mActionType;
		private String mTitle;
		private String mText;
		private String mOkTitle;
		private String mCancelTitle;
		private String mUrl;

		public Action(int actionID, int actionType, String title, String text, String okTitle,
				String cancelTitle, String url) {
			super();
			mActionID = actionID;
			mActionType = actionType;
			mTitle = title;
			mText = text;
			mOkTitle = okTitle;
			mCancelTitle = cancelTitle;
			mUrl = url;
		}

		public int getActionID() {
			return mActionID;
		}

		public void setActionID(int actionID) {
			mActionID = actionID;
		}

		public int getActionType() {
			return mActionType;
		}

		public void setActionType(int actionType) {
			mActionType = actionType;
		}

		public String getTitle() {
			return mTitle;
		}

		public void setTitle(String title) {
			mTitle = title;
		}

		public String getText() {
			return mText;
		}

		public void setText(String text) {
			mText = text;
		}

		public String getOkTitle() {
			return mOkTitle;
		}

		public void setOkTitle(String okTitle) {
			mOkTitle = okTitle;
		}

		public String getCancelTitle() {
			return mCancelTitle;
		}

		public void setCancelTitle(String cancelTitle) {
			mCancelTitle = cancelTitle;
		}

		public String getUrl() {
			return mUrl;
		}

		public void setUrl(String url) {
			mUrl = url;
		}

	}
}
