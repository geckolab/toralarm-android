
package com.eisterhues_media_2.network.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

public class Table {

	private List<Competition> mCompetitions = new ArrayList<Competition>();

	public Table(JSONArray data) throws JSONException {
		for (int i = 0, size = data.length(); i < size; i++)
			mCompetitions.add(new Competition(data.getJSONArray(i)));
	}

	public List<Competition> getCompetitions() {
		return mCompetitions;
	}

	public void setCompetitions(List<Competition> competitions) {
		mCompetitions = competitions;
	}

	public class Competition {

		private List<Team> mTeams = new ArrayList<Team>();

		public Competition(JSONArray data) throws JSONException {
			JSONArray t = null;

			for (int i = 0, size = data.length(); i < size; i++) {
				t = data.getJSONArray(i);
				mTeams.add(new Team(t.getString(0), t.getString(1), t.getString(2), t
						.getString(3), t.getString(4), t.getString(5)));
			}
		}

		public List<Team> getTeams() {
			return mTeams;
		}

		public void setTeams(List<Team> teams) {
			mTeams = teams;
		}

	}

	public class Team {

		private String mTeamName;
		private String mTeamID;
		private String mTeamMatches;
		private String mGoalDifference;
		private String mPoints;
		private String mPosColor;

		public Team(String teamName, String teamID, String teamMatches, String goalDifference,
				String points, String posColor) {
			mTeamName = teamName;
			mTeamID = teamID;
			mTeamMatches = teamMatches;
			mGoalDifference = goalDifference;
			mPoints = points;
			mPosColor = posColor;
		}

		public String getTeamName() {
			return mTeamName;
		}

		public void setTeamName(String teamName) {
			mTeamName = teamName;
		}

		public String getTeamID() {
			return mTeamID;
		}

		public void setTeamID(String teamID) {
			mTeamID = teamID;
		}

		public String getTeamMatches() {
			return mTeamMatches;
		}

		public void setTeamMatches(String teamMatches) {
			mTeamMatches = teamMatches;
		}

		public String getGoalDifference() {
			return mGoalDifference;
		}

		public void setGoalDifference(String goalDifference) {
			mGoalDifference = goalDifference;
		}

		public String getPoints() {
			return mPoints;
		}

		public void setPoints(String points) {
			mPoints = points;
		}

		public String getPosColor() {
			return mPosColor;
		}

		public void setPosColor(String posColor) {
			mPosColor = posColor;
		}

	}

}
