
package com.eisterhues_media_2.network.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MatchDetails {
	private String mMatchID;
	private Data mData = null;
	private List<Detail> mDetails = new ArrayList<Detail>();
	private Quotas mQuotas = null;

	public MatchDetails(JSONObject data) throws JSONException {
		JSONArray d = data.getJSONArray("data");
		JSONArray details = null;
		JSONArray detailst = null;
		JSONArray quotas = null;

		mMatchID = data.getString("match_id");

		mData = new Data(d.getString(0), d.getString(1), d.getString(2), d
				.getString(3), d.getString(4), d.getString(5), d.getString(6), d
				.getString(7), d.getString(8));

		try {
			details = data.getJSONArray("details");
			for (int i = 0, size = details.length(); i < size; i++) {
				detailst = details.getJSONArray(i);
				mDetails.add(new Detail(detailst.getString(0), detailst.getString(2), detailst
						.getString(3), detailst
						.getString(4), detailst.getString(5), detailst.getString(6)));
			}
		} catch (JSONException e) {
		}

		if (data.has("quotas")) {
			quotas = data.getJSONArray("quotas");
			mQuotas = new Quotas(quotas.getString(0), quotas.getString(1), quotas.getString(2),
					quotas
							.getString(3));
		}

	}

	public String getMatchID() {
		return mMatchID;
	}

	public void setMatchID(String matchID) {
		mMatchID = matchID;
	}

	public Data getData() {
		return mData;
	}

	public void setData(Data data) {
		mData = data;
	}

	public List<Detail> getDetails() {
		return mDetails;
	}

	public void setDetails(List<Detail> details) {
		mDetails = details;
	}

	public Quotas getQuotas() {
		return mQuotas;
	}

	public void setQuotas(Quotas quotas) {
		mQuotas = quotas;
	}

	public class Data {
		private String mTime;
		private String mTeam1ID;
		private String mTeam1;
		private String mTeam2ID;
		private String mTeam2;
		private String mTeam1Goals;
		private String mTeam2Goals;
		private String mMatchState;
		private String mRoundText;

		public Data(String time, String team1id, String team1, String team2id, String team2,
				String team1Goals, String team2Goals, String matchState, String roundText) {
			mTime = time;
			mTeam1ID = team1id;
			mTeam1 = team1;
			mTeam2ID = team2id;
			mTeam2 = team2;
			mTeam1Goals = team1Goals;
			mTeam2Goals = team2Goals;
			mMatchState = matchState;
			mRoundText = roundText;
		}

		public String getTime() {
			return mTime;
		}

		public void setTime(String time) {
			mTime = time;
		}

		public String getTeam1ID() {
			return mTeam1ID;
		}

		public void setTeam1ID(String team1id) {
			mTeam1ID = team1id;
		}

		public String getTeam1() {
			return mTeam1;
		}

		public void setTeam1(String team1) {
			mTeam1 = team1;
		}

		public String getTeam2ID() {
			return mTeam2ID;
		}

		public void setTeam2ID(String team2id) {
			mTeam2ID = team2id;
		}

		public String getTeam2() {
			return mTeam2;
		}

		public void setTeam2(String team2) {
			mTeam2 = team2;
		}

		public String getTeam1Goals() {
			return mTeam1Goals;
		}

		public void setTeam1Goals(String team1Goals) {
			mTeam1Goals = team1Goals;
		}

		public String getTeam2Goals() {
			return mTeam2Goals;
		}

		public void setTeam2Goals(String team2Goals) {
			mTeam2Goals = team2Goals;
		}

		public String getMatchState() {
			return mMatchState;
		}

		public void setMatchState(String matchState) {
			mMatchState = matchState;
		}

		public String getRoundText() {
			return mRoundText;
		}

		public void setRoundText(String roundText) {
			mRoundText = roundText;
		}

	}

	public class Detail {
		private String mTime;
		private String mTeam;
		private String mPlayerName;
		private String mTeam1Goals;
		private String mTeam2Goals;
		private String mType;

		public Detail(String time, String team, String playerName, String team1Goals,
				String team2Goals, String type) {
			mTime = time;
			mTeam = team;
			mPlayerName = playerName;
			mTeam1Goals = team1Goals;
			mTeam2Goals = team2Goals;
			mType = type;
		}

		public String getTime() {
			return mTime;
		}

		public void setTime(String time) {
			mTime = time;
		}

		public String getTeam() {
			return mTeam;
		}

		public void setTeam(String team) {
			mTeam = team;
		}

		public String getPlayerName() {
			return mPlayerName;
		}

		public void setPlayerName(String playerName) {
			mPlayerName = playerName;
		}

		public String getTeam1Goals() {
			return mTeam1Goals;
		}

		public void setTeam1Goals(String team1Goals) {
			mTeam1Goals = team1Goals;
		}

		public String getTeam2Goals() {
			return mTeam2Goals;
		}

		public void setTeam2Goals(String team2Goals) {
			mTeam2Goals = team2Goals;
		}

		public String getType() {
			return mType;
		}

		public void setType(String type) {
			mType = type;
		}

	}

	public class Quotas {
		private String mQuotaTeam1;
		private String mQuotaX;
		private String mQuotaTeam2;
		private String mUrl;

		public Quotas(String quotaTeam1, String quotaX, String quotaTeam2, String url) {
			mQuotaTeam1 = quotaTeam1;
			mQuotaX = quotaX;
			mQuotaTeam2 = quotaTeam2;
			mUrl = url;
		}

		public String getQuotaTeam1() {
			return mQuotaTeam1;
		}

		public void setQuotaTeam1(String quotaTeam1) {
			mQuotaTeam1 = quotaTeam1;
		}

		public String getQuotaX() {
			return mQuotaX;
		}

		public void setQuotaX(String quotaX) {
			mQuotaX = quotaX;
		}

		public String getQuotaTeam2() {
			return mQuotaTeam2;
		}

		public void setQuotaTeam2(String quotaTeam2) {
			mQuotaTeam2 = quotaTeam2;
		}

		public String getUrl() {
			return mUrl;
		}

		public void setUrl(String url) {
			mUrl = url;
		}

	}
}
