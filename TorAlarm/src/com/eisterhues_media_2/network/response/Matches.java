
package com.eisterhues_media_2.network.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Matches {

	private List<Round> mRound = new ArrayList<Round>();
	private List<String> mRounds = new ArrayList<String>();
	private int mCurrentRound;
	private int mSelectableRound;

	public Matches(JSONObject data) throws JSONException {
		JSONArray round = data.getJSONArray("round");
		JSONArray rounds = data.getJSONArray("rounds");

		for (int i = 0, size = round.length(); i < size; i++)
			mRound.add(new Round(round.getJSONObject(i)));

		for (int i = 0, size = rounds.length(); i < size; i++)
			mRounds.add(rounds.getString(i));

		mCurrentRound = data.getInt("current_round");
		mSelectableRound = data.getInt("selectable_rounds");

	}

	public List<Round> getRound() {
		return mRound;
	}

	public void setRound(List<Round> round) {
		mRound = round;
	}

	public List<String> getRounds() {
		return mRounds;
	}

	public void setRounds(List<String> rounds) {
		mRounds = rounds;
	}

	public int getCurrentRound() {
		return mCurrentRound;
	}

	public void setCurrentRound(int currentRound) {
		mCurrentRound = currentRound;
	}
	
	public int getSelectableRound() {
		return mSelectableRound;
	}

	public void setSelectableRound(int selectableRound) {
		mSelectableRound = selectableRound;
	}


	public class Round {
		private String mTime;
		private List<Data> mData = new ArrayList<Data>();

		public Round(JSONObject data) throws JSONException {
			JSONArray d = data.getJSONArray("data");
			JSONArray dt = null;

			mTime = data.getString("time");

			for (int i = 0, size = d.length(); i < size; i++) {
				dt = d.getJSONArray(i);
				mData.add(new Data(dt.getString(0), dt.getString(1), dt.getString(2), dt
						.getString(3), dt.getString(4), dt.getString(5), dt.getString(6), dt
						.getString(7), dt.getString(8)));
			}
		}

		public String getTime() {
			return mTime;
		}

		public void setTime(String time) {
			mTime = time;
		}

		public List<Data> getData() {
			return mData;
		}

		public void setData(List<Data> data) {
			mData = data;
		}

	}

	public class Data {
		private String mMatchID;
		private String mTeam1ID;
		private String mTeam1;
		private String mTeam2ID;
		private String mTeam2;
		private String mTeam1Goals;
		private String mTeam2Goals;
		private String mMatchState;
		private String mRoundText;

		public Data(String matchID, String team1id, String team1, String team2id, String team2,
				String team1Goals, String team2Goals, String matchState, String roundText) {
			mMatchID = matchID;
			mTeam1ID = team1id;
			mTeam1 = team1;
			mTeam2ID = team2id;
			mTeam2 = team2;
			mTeam1Goals = team1Goals;
			mTeam2Goals = team2Goals;
			mMatchState = matchState;
			mRoundText = roundText;
		}

		public String getMatchID() {
			return mMatchID;
		}

		public void setMatchID(String matchID) {
			mMatchID = matchID;
		}

		public String getTeam1ID() {
			return mTeam1ID;
		}

		public void setTeam1ID(String team1id) {
			mTeam1ID = team1id;
		}

		public String getTeam1() {
			return mTeam1;
		}

		public void setTeam1(String team1) {
			mTeam1 = team1;
		}

		public String getTeam2ID() {
			return mTeam2ID;
		}

		public void setTeam2ID(String team2id) {
			mTeam2ID = team2id;
		}

		public String getTeam2() {
			return mTeam2;
		}

		public void setTeam2(String team2) {
			mTeam2 = team2;
		}

		public String getTeam1Goals() {
			return mTeam1Goals;
		}

		public void setTeam1Goals(String team1Goals) {
			mTeam1Goals = team1Goals;
		}

		public String getTeam2Goals() {
			return mTeam2Goals;
		}

		public void setTeam2Goals(String team2Goals) {
			mTeam2Goals = team2Goals;
		}

		public String getMatchState() {
			return mMatchState;
		}

		public void setMatchState(String matchState) {
			mMatchState = matchState;
		}

		public String getRoundText() {
			return mRoundText;
		}

		public void setRoundText(String roundText) {
			mRoundText = roundText;
		}

	}
}
