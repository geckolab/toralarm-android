
package com.eisterhues_media_2.network.response;

import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetPushgroup {

	private Hashtable<String, Integer> mLiga1 = new Hashtable<String, Integer>();
	private Hashtable<String, Integer> mLiga2 = new Hashtable<String, Integer>();
	private Hashtable<String, Integer> mLiga3 = new Hashtable<String, Integer>();
	private Hashtable<String, Integer> mInternational = new Hashtable<String, Integer>();

	public GetPushgroup(JSONObject data) throws JSONException {
		JSONArray liga1 = data.getJSONArray("1. Liga");
		JSONArray liga2 = data.getJSONArray("2. Liga");
		JSONArray liga3 = data.getJSONArray("3. Liga");
		JSONArray international = data.getJSONArray("International");
		JSONArray tmp = null;
		
		for (int i = 0, size = liga1.length(); i < size; i++) {
			tmp = liga1.getJSONArray(i);
			mLiga1.put(tmp.getString(0), tmp.getInt(1));
		}
		
		for (int i = 0, size = liga2.length(); i < size; i++) {
			tmp = liga2.getJSONArray(i);
			mLiga2.put(tmp.getString(0), tmp.getInt(1));
		}
		
		for (int i = 0, size = liga3.length(); i < size; i++) {
			tmp = liga3.getJSONArray(i);
			mLiga3.put(tmp.getString(0), tmp.getInt(1));
		}
		
		for (int i = 0, size = international.length(); i < size; i++) {
			tmp = international.getJSONArray(i);
			mInternational.put(tmp.getString(0), tmp.getInt(1));
		}
	}

	public Hashtable<String, Integer> getLiga1() {
		return mLiga1;
	}

	public void setLiga1(Hashtable<String, Integer> liga1) {
		mLiga1 = liga1;
	}

	public Hashtable<String, Integer> getLiga2() {
		return mLiga2;
	}

	public void setLiga2(Hashtable<String, Integer> liga2) {
		mLiga2 = liga2;
	}

	public Hashtable<String, Integer> getLiga3() {
		return mLiga3;
	}

	public void setLiga3(Hashtable<String, Integer> liga3) {
		mLiga3 = liga3;
	}

	public Hashtable<String, Integer> getInternational() {
		return mInternational;
	}

	public void setInternational(Hashtable<String, Integer> international) {
		mInternational = international;
	}

}
