
package com.eisterhues_media_2.network.input;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class InputParams {

	public static List<BasicNameValuePair> matches(int competition_id, String language,
			int selected_round, long timestamp) {
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("type", "matches"));
		params.add(new BasicNameValuePair("competition", String.valueOf(competition_id)));
		params.add(new BasicNameValuePair("lng", language));
		if (selected_round != -1)
			params.add(new BasicNameValuePair("round", String.valueOf(selected_round)));
		params.add(new BasicNameValuePair("last_sync", String.valueOf(timestamp)));

		return params;
	}

	public static List<BasicNameValuePair> matchDetails(int match_id, String language,
			long timestamp) {
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("type", "details"));
		params.add(new BasicNameValuePair("match_id", String.valueOf(match_id)));
		params.add(new BasicNameValuePair("lng", language));
		params.add(new BasicNameValuePair("last_sync", String.valueOf(timestamp)));

		return params;
	}

	public static List<BasicNameValuePair> table(int competition_id, String language, long timestamp) {
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("type", "table"));
		params.add(new BasicNameValuePair("competition", String.valueOf(competition_id)));
		params.add(new BasicNameValuePair("lng", language));
		params.add(new BasicNameValuePair("last_sync", String.valueOf(timestamp)));

		return params;
	}

	public static List<BasicNameValuePair> getPushgroup(int pushgroup_id, String language) {
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("type", "settings"));
		params.add(new BasicNameValuePair("pushgroup", String.valueOf(pushgroup_id)));
		params.add(new BasicNameValuePair("lng", language));

		return params;
	}

	public static JSONObject settings(String token, JSONObject settings, int type, String language,
			String appVersion, String os, String device, int tablet, int lastAction, int appStarts) {
		JSONObject params = new JSONObject();
		try {
			params.put("token", token);
			params.put("settings", settings);
			params.put("type", String.valueOf(type));
			params.put("lng", language);
			params.put("app_version", appVersion);
			params.put("os", os);
			params.put("device", device);
			params.put("tablet", String.valueOf(tablet));
			params.put("last_action", String.valueOf(lastAction));
			params.put("app_starts", String.valueOf(appStarts));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return params;
	}

	public static JSONObject bugReport(String token, JSONObject settings, int type,
			String language, String appVersion, String os, String device, int tablet,
			int lastAction, int appStarts, String email, String message, int isSupport) {
		JSONObject params = new JSONObject();
		try {
			params.put("token", token);
			params.put("settings", settings);
			params.put("type", String.valueOf(type));
			params.put("lng", language);
			params.put("app_version", appVersion);
			params.put("os", os);
			params.put("device", device);
			params.put("tablet", String.valueOf(tablet));
			params.put("last_action", String.valueOf(lastAction));
			params.put("app_starts", String.valueOf(appStarts));
			params.put("email", email);
			params.put("message", message);
			params.put("is_support", String.valueOf(isSupport));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return params;
	}
}
