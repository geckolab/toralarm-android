package com.eisterhues_media_2.network;

import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public interface NetworkHelperInterface {
	public List<BasicNameValuePair> setApiInputParams();
	public void apiResultAction(String result);
}
