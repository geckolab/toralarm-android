
package com.eisterhues_media_2.activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.ButterKnife;

import com.eisterhues_media_2.ImpessumActivity;
import com.eisterhues_media_2.R;
import com.eisterhues_media_2.utils.LogUtils;
import com.eisterhues_media_2.utils.PrefsManager;

@SuppressLint("NewApi")
public class BaseActivity extends ActionBarActivity {

	public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION = "com.eisterhues_media_2.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";
	private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
	public static final IntentFilter INTENT_FILTER = createIntentFilter();

	private PrefsManager mPrefsManager;
	private View mCustomView;
	private TextView mTitle;
	private ImageButton mMenuButton;
	private ImageButton mBackButton;
	private ImageButton mSettingsButton;
	private ImageButton mReloadButton;
	private ImageButton mParagraphButton;
	private Spinner mSpinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(layout());
		ButterKnife.inject(this);

		registerBaseActivityReceiver();

		mPrefsManager = new PrefsManager(this);

		ActionBar mActionBar = getActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		mActionBar.setDisplayOptions(0);
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		LayoutInflater mInflater = LayoutInflater.from(this);

		mCustomView = mInflater.inflate(R.layout.actionbar, null);

		mTitle = (TextView) mCustomView.findViewById(R.id.title_text);
		mSpinner = (Spinner) mCustomView.findViewById(R.id.spinner);
		mMenuButton = (ImageButton) mCustomView.findViewById(R.id.menu);
		mBackButton = (ImageButton) mCustomView.findViewById(R.id.back);
		mSettingsButton = (ImageButton) mCustomView.findViewById(R.id.settings);
		mReloadButton = (ImageButton) mCustomView.findViewById(R.id.reload);
		mParagraphButton = (ImageButton) mCustomView.findViewById(R.id.paragraph);
		mParagraphButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(BaseActivity.this, ImpessumActivity.class);
				startActivity(i);
			}
		});

		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
	}

	public int layout() {
		return 0;
	}

	public PrefsManager getPrefsManager() {
		return mPrefsManager;
	}

	public TextView getTitleView() {
		return mTitle;
	}

	public ImageButton getSettingsButton() {
		return mSettingsButton;
	}

	public ImageButton getMenuButton() {
		return mMenuButton;
	}

	public ImageButton getReloadButton() {
		return mReloadButton;
	}

	public ImageButton getParagraphButton() {
		return mParagraphButton;
	}

	public Spinner getSpinner() {
		return mSpinner;
	}

	public void setSpinner(Spinner spinner) {
		mSpinner = spinner;
	}

	public ImageButton getBackButton() {
		return mBackButton;
	}

	public String getLanguage() {
		return mPrefsManager.getLanguage();
	}

	private static IntentFilter createIntentFilter() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
		return filter;
	}

	protected void registerBaseActivityReceiver() {
		registerReceiver(baseActivityReceiver, INTENT_FILTER);
	}

	protected void unRegisterBaseActivityReceiver() {
		unregisterReceiver(baseActivityReceiver);
	}

	public class BaseActivityReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)) {
				LogUtils.d("FINISH", "ACTIVITY");
				finish();
			}
		}
	}

	protected void closeAllActivities() {
		sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
	}
}
