package com.eisterhues_media_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import butterknife.InjectView;
import butterknife.Optional;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.fragment.AddFreeFragment;
import com.eisterhues_media_2.fragment.PushFragment;
import com.eisterhues_media_2.fragment.SettingsFragment;
import com.eisterhues_media_2.fragment.SoundFragment;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;

public class SettingsFragmentActivity extends BaseActivity {

	private SettingsFragment settingsFragment;
	private PushFragment pushFragment;
	private SoundFragment soundFragment;
	private AddFreeFragment addFreeFragment;

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@Override
	public int layout() {
		return R.layout.activity_settings_fragment;
	}

	@Optional
	@InjectView(R.id.fragment_left)
	ViewGroup mLeft;

	@Optional
	@InjectView(R.id.fragment_right)
	ViewGroup mRight;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getTitleView().setText(getString(R.string.settings));

		getMenuButton().setVisibility(View.VISIBLE);
		getMenuButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(SettingsFragmentActivity.this,
						MainActivity.class);
				startActivity(i);
			}
		});

		getParagraphButton().setVisibility(View.VISIBLE);

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		if (savedInstanceState != null) {

		} else {
			if (getIntent().getExtras().getBoolean(Const.ADD_REQUEST, false)) {
				if (mRight == null) {
					addFreeFragment = new AddFreeFragment();
					FragmentTransaction fragmentTransaction = getSupportFragmentManager()
							.beginTransaction();
					fragmentTransaction.replace(mLeft.getId(), addFreeFragment,
							AddFreeFragment.class.getName());

					fragmentTransaction.commit();
				} else {
					if (mLeft != null) {
						settingsFragment = new SettingsFragment();
						FragmentTransaction fragmentTransaction = getSupportFragmentManager()
								.beginTransaction();
						fragmentTransaction.replace(mLeft.getId(),
								settingsFragment,
								SettingsFragment.class.getName());

						fragmentTransaction.commit();
					}

					if (mRight != null) {
						addFreeFragment = new AddFreeFragment();
						FragmentTransaction fragmentTransaction = getSupportFragmentManager()
								.beginTransaction();
						fragmentTransaction.replace(mRight.getId(),
								addFreeFragment,
								AddFreeFragment.class.getName());

						fragmentTransaction.commit();
					}
				}
			} else {
				if (mLeft != null) {
					settingsFragment = new SettingsFragment();
					FragmentTransaction fragmentTransaction = getSupportFragmentManager()
							.beginTransaction();
					fragmentTransaction.replace(mLeft.getId(),
							settingsFragment, SettingsFragment.class.getName());

					fragmentTransaction.commit();
				}

				if (mRight != null) {
					pushFragment = new PushFragment(Const.PUSH_DE);
					FragmentTransaction fragmentTransaction = getSupportFragmentManager()
							.beginTransaction();
					fragmentTransaction.replace(mRight.getId(), pushFragment,
							PushFragment.class.getName());

					fragmentTransaction.commit();
				}
			}
		}
	}

	public void settings() {
		if (mRight == null) {
			settingsFragment = new SettingsFragment();
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(mLeft.getId(), settingsFragment,
					SettingsFragment.class.getName());

			fragmentTransaction.commit();
		} else if (mRight != null) {
			settingsFragment = new SettingsFragment();
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(mRight.getId(), settingsFragment,
					SettingsFragment.class.getName());

			fragmentTransaction.commit();
		}
	}

	public void push(int pushID) {
		if (mRight == null) {
			pushFragment = new PushFragment(pushID);
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(mLeft.getId(), pushFragment,
					PushFragment.class.getName());

			fragmentTransaction.commit();
		} else if (mRight != null) {
			pushFragment = new PushFragment(pushID);
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(mRight.getId(), pushFragment,
					PushFragment.class.getName());

			fragmentTransaction.commit();
		}
	}

	public void sound() {
		if (mRight == null) {
			soundFragment = new SoundFragment();
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(mLeft.getId(), soundFragment,
					SoundFragment.class.getName());

			fragmentTransaction.commit();
		} else if (mRight != null) {
			soundFragment = new SoundFragment();
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(mRight.getId(), soundFragment,
					SoundFragment.class.getName());

			fragmentTransaction.commit();
		}
	}

	public void ad() {
		if (mRight == null) {
			addFreeFragment = new AddFreeFragment();
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(mLeft.getId(), addFreeFragment,
					AddFreeFragment.class.getName());

			fragmentTransaction.commit();
		} else if (mRight != null) {
			addFreeFragment = new AddFreeFragment();
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(mRight.getId(), addFreeFragment,
					AddFreeFragment.class.getName());

			fragmentTransaction.commit();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}

	public ViewGroup getLeft() {
		return mLeft;
	}

	public void setLeft(ViewGroup left) {
		mLeft = left;
	}

	public ViewGroup getRight() {
		return mRight;
	}

	public void setRight(ViewGroup right) {
		mRight = right;
	}

}
