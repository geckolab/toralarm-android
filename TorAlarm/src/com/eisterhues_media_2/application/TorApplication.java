
package com.eisterhues_media_2.application;

import android.app.Application;

import com.eisterhues_media_2.utils.Const;
import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.PlacementSize;

public class TorApplication extends Application {
	private int bannerPlacementId = -1;
	private int fullscreenPlacementId = -1;

	public void onCreate() {
		super.onCreate();

		AATKit.init(this, null);
		if (Const.AATKIT) {
			bannerPlacementId = AATKit.createPlacement("Banner",
					PlacementSize.Banner320x53);
			fullscreenPlacementId = AATKit.createPlacement("fullscreen",
					PlacementSize.Fullscreen);
		}
	}

	public int getBannerPlacementId() {
		return bannerPlacementId;
	}

	public int getFullscreenPlacementId() {
		return fullscreenPlacementId;
	}
}
