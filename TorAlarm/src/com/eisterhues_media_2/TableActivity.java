
package com.eisterhues_media_2;

import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.InjectView;
import butterknife.OnClick;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.network.NetworkHelper;
import com.eisterhues_media_2.network.NetworkHelperInterface;
import com.eisterhues_media_2.network.input.InputParams;
import com.eisterhues_media_2.network.response.Table;
import com.eisterhues_media_2.network.response.Table.Competition;
import com.eisterhues_media_2.network.response.Table.Team;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.NetworkUtils;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;
import com.squareup.picasso.Picasso;

public class TableActivity extends BaseActivity {

	private Handler handler = new Handler();
	private Runnable action = null;

	private NetworkHelper mNetworkHelper;
	private LayoutInflater mInflater;
	private View mTableHeaderView;
	private View mTableDetailsView;
	private int mSelectedRound;
	private int mCompetitionID;

	private String mCompetition;

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@InjectView(R.id.linearLayoutMatches)
	LinearLayout mMatches;

	@InjectView(R.id.swipe_container)
	SwipeRefreshLayout mRefresh;

	@Override
	public int layout() {
		return R.layout.activity_table;
	}

	@OnClick(R.id.competition)
	public void send(View view) {
		Intent i = new Intent(TableActivity.this, CompetitionActivity.class);
		i.putExtra(Const.COMPETITION_TYPE, mCompetition);
		i.putExtra(Const.SELECTED_ROUND, mSelectedRound);
		startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mRefresh.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {

				mRefresh.setOnRefreshListener(this);
				mRefresh
						.setColorScheme(android.R.color.white,
								android.R.color.holo_green_light,
								android.R.color.white,
								android.R.color.holo_green_light);

				getData(true);
			}

		});

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		mInflater = LayoutInflater.from(this);

		mCompetition = getIntent().getExtras().getString(Const.COMPETITION_TYPE);
		mSelectedRound = getIntent().getExtras().getInt(Const.SELECTED_ROUND);

		if (mCompetition.equals(Const.LIGA1)) {
			getTitleView().setText(getString(R.string.liga1));
			mCompetitionID = 0;
		} else if (mCompetition.equals(Const.LIGA2)) {
			getTitleView().setText(getString(R.string.liga2));
			mCompetitionID = 0;
		} else if (mCompetition.equals(Const.LIGA3)) {
			getTitleView().setText(getString(R.string.liga3));
			mCompetitionID = 0;
		} else if (mCompetition.equals(Const.CL)) {
			getTitleView().setText(getString(R.string.cl));
			mCompetitionID = 5;
		} else if (mCompetition.equals(Const.EL)) {
			getTitleView().setText(getString(R.string.el));
			mCompetitionID = 5;
		} else if (mCompetition.equals(Const.POKAL)) {
			getTitleView().setText(getString(R.string.pokal));
			mCompetitionID = 3;
		} else if (mCompetition.equals(Const.LAND)) {
			getTitleView().setText(getString(R.string.land));
			mCompetitionID = 4;
		}

		getMenuButton().setVisibility(View.VISIBLE);
		getMenuButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(TableActivity.this, MainActivity.class);
				startActivity(i);
			}
		});

		getReloadButton().setVisibility(View.VISIBLE);
		getReloadButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				getData(true);
			}
		});

		getData(false);

		action = new Runnable() {
			@Override
			public void run() {
				getData(false);
				handler.postDelayed(action, 30000);
			}
		};
		handler.postDelayed(action, 30000);

	}

	@Override
	protected void onStop() {
		handler.removeCallbacks(action);
		super.onStart();
	}

	private void getData(final boolean refresh) {
		if (!NetworkUtils.isNetworkAvailable(this) && refresh)
			Toast.makeText(getApplicationContext(),
					getString(R.string.datanotloaded),
					Toast.LENGTH_SHORT).show();

		mNetworkHelper = new NetworkHelper();
		mNetworkHelper.sentApiRequest(this, new NetworkHelperInterface() {

			@Override
			public List<BasicNameValuePair> setApiInputParams() {
				return InputParams.table(mCompetitionID, getLanguage(), 0);
			}

			@Override
			public void apiResultAction(String result) {
				try {
					Table table = new Table(new JSONArray(result));

					mMatches.removeAllViews();

					int i = 1;
					int g = 1;
					for (Competition c : table.getCompetitions()) {
						mTableHeaderView = mInflater.inflate(R.layout.round_header, null);

						TextView right = (TextView) mTableHeaderView.findViewById(R.id.text_right);
						right.setText(getString(R.string.table_header));

						TextView left = (TextView) mTableHeaderView.findViewById(R.id.text_left);
						left.setText("Group " + g);

						mMatches.addView(mTableHeaderView);

						i = 1;
						for (Team t : c.getTeams()) {
							mTableDetailsView = mInflater.inflate(R.layout.table_details, null);

							if (i % 2 != 0) {
								mTableDetailsView.setBackgroundColor(Color.parseColor("#000000"));
								mTableDetailsView.getBackground().setAlpha(128);
							}

							TextView time = (TextView) mTableDetailsView
									.findViewById(R.id.text_time);
							time.setText(String.valueOf(i));

							TextView name = (TextView) mTableDetailsView
									.findViewById(R.id.text_name);
							name.setText(t.getTeamName());

							ImageView teamIcon = (ImageView) mTableDetailsView
									.findViewById(R.id.team_icon);
							Picasso.with(TableActivity.this)
									.load(Utils.getIconURL(t.getTeamName()))
									.into(teamIcon);

							TextView s = (TextView) mTableDetailsView
									.findViewById(R.id.text_score);
							s.setText(t.getTeamMatches() + "     " + t.getGoalDifference()
									+ "     "
									+ t.getPoints() + "   ");

							ImageView icon = (ImageView) mTableDetailsView
									.findViewById(R.id.icon);

							if (t.getPosColor().equals("0")) {
								Picasso.with(TableActivity.this).load(R.drawable.icon_green)
										.into(icon);
							} else if (t.getPosColor().equals("1")) {
								Picasso.with(TableActivity.this).load(R.drawable.icon_bright_green)
										.into(icon);
							} else if (t.getPosColor().equals("2")) {
								Picasso.with(TableActivity.this).load(R.drawable.icon_light_blue)
										.into(icon);
							} else if (t.getPosColor().equals("3")) {
								Picasso.with(TableActivity.this).load(R.drawable.icon_blue)
										.into(icon);
							} else if (t.getPosColor().equals("4")) {
								Picasso.with(TableActivity.this).load(R.drawable.icon_orange)
										.into(icon);
							} else if (t.getPosColor().equals("5")) {
								Picasso.with(TableActivity.this).load(R.drawable.icon_red)
										.into(icon);
							}

							i++;
							mMatches.addView(mTableDetailsView);
						}
						g++;
					}

					if (refresh) {
						Toast.makeText(getApplicationContext(),
								getString(R.string.dataloaded),
								Toast.LENGTH_SHORT).show();

						mRefresh.setRefreshing(false);
					}

				} catch (JSONException e) {
				}
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}
}
