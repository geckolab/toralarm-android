
package com.eisterhues_media_2.gcm;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.eisterhues_media_2.MainActivity;
import com.eisterhues_media_2.R;
import com.eisterhues_media_2.utils.LogUtils;
import com.eisterhues_media_2.utils.PrefsManager;
import com.eisterhues_media_2.utils.Utils;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCMNotificationIntentService extends IntentService {

	public static final String TAG = "GCMNotificationIntentService";

	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	private PrefsManager mPrefsManager;

	public GCMNotificationIntentService() {
		super("GcmIntentService");

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		mPrefsManager = new PrefsManager(this);
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty() && mPrefsManager.isPushNotificationEnable()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {

			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {

			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType))
				try {
					{

						for (int i = 0; i < 3; i++) {
							LogUtils.i(TAG, "Working... " + (i + 1) + "/5 @ "
									+ SystemClock.elapsedRealtime());
							try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
							}

						}
						LogUtils.i(
								TAG,
								"Completed work @ "
										+ SystemClock.elapsedRealtime());

						if (extras.get("type").equals("1")) {
							sendNotification(extras);
						} else if (extras.get("type").equals("0")) {
							sendCustomNotification(extras);
						}

						if ((mPrefsManager.getNotyficationSound() == 0 && mPrefsManager
								.isPushVibEnable())
								|| (mPrefsManager.getNotyficationSound() > 0 && !mPrefsManager
										.isPushMuteEnable()))
							Utils.play(this, mPrefsManager.getNotyficationSound());

					}
				} catch (NotFoundException e) {
					e.printStackTrace();
				}
		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(Bundle extras) {
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, MainActivity.class), 0);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(extras.get("title").toString())
				.setStyle(
						new NotificationCompat.BigTextStyle()
								.bigText(extras.get("text").toString()))
				.setContentText(extras.get("text").toString()).setAutoCancel(true);

		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(Integer.valueOf(extras.get("match_id").toString()),
				mBuilder.build());
		LogUtils.d(TAG, "Notification sent successfully.");
	}

	private void sendCustomNotification(Bundle extras) {
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Notification notification = new Notification(R.drawable.ic_launcher, extras.get("title")
				.toString(), System.currentTimeMillis());

		NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.push);
		contentView.setImageViewBitmap(R.id.team1_icon,
				ImageOperations(this, Utils.getIconURL(extras.get("name_team1").toString())));
		contentView.setImageViewBitmap(R.id.team2_icon,
				ImageOperations(this, Utils.getIconURL(extras.get("name_team2").toString())));
		contentView.setTextViewText(R.id.title, extras.get("title").toString());
		contentView.setTextViewText(R.id.text_left, extras.get("name_team1").toString());
		contentView.setTextViewText(R.id.text_right, extras.get("name_team2").toString());
		contentView.setTextViewText(R.id.text_score, extras.get("score_team1").toString() + " : "
				+ extras.get("score_team2").toString());
		notification.contentView = contentView;

		Intent notificationIntent = new Intent(this, MainActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.contentIntent = contentIntent;

		mNotificationManager.notify(Integer.valueOf(extras.get("match_id").toString()),
				notification);
	}

	private Bitmap ImageOperations(Context ctx, String url) {
		try {
			InputStream is = (InputStream) this.fetch(url);
			return BitmapFactory.decodeStream(is);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Object fetch(String address) throws MalformedURLException, IOException {
		URL url = new URL(address);
		Object content = url.getContent();
		return content;
	}
}
