
package com.eisterhues_media_2.gcm;

import java.io.IOException;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.LogUtils;
import com.eisterhues_media_2.utils.PrefsManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCM {

	private static String TAG = "GCM Registration";

	private GoogleCloudMessaging mGCM;
	private static Context mContext;
	private String mRegId;
	private PrefsManager mPrefsManager;
	private GCMCallback mGCMCallback;

	public GCM(Context context, PrefsManager prefsManager, GCMCallback GCMCallback) {
		mContext = context;
		mPrefsManager = prefsManager;
		mGCMCallback = GCMCallback;
	}

	public void registerGCM() {
		mGCM = GoogleCloudMessaging.getInstance(mContext);
		mRegId = getRegistrationId();

		if (TextUtils.isEmpty(mRegId)) {
			registerInBackground();

			LogUtils.d(TAG, "Successfully registered with GCM server - regId: "
					+ mRegId);
		} else {
			LogUtils.d(TAG, "Device already registered - regId: " + mRegId);
			mGCMCallback.onGetRegID();
		}
	}

	private String getRegistrationId() {
		String registrationId = mPrefsManager.getDeviceID();
		if (registrationId.isEmpty()) {
			LogUtils.d(TAG, "Registration not found.");
			return "";
		}

		int registeredVersion = mPrefsManager.getAppVersion();
		int currentVersion = getAppVersion(mContext);
		if (registeredVersion != currentVersion) {
			LogUtils.d(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			LogUtils.d(TAG, "Error: " + e);
			throw new RuntimeException(e);
		}
	}

	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (mGCM == null) {
						mGCM = GoogleCloudMessaging.getInstance(mContext);
					}
					mRegId = mGCM.register(Const.GOOGLE_PROJECT_ID);
					LogUtils.d(TAG, "registerInBackground - regId: " + mRegId);
					msg = "Device registered, registration ID=" + mRegId;

					storeRegistrationId(mRegId);
				} catch (IOException ex) {
					msg = ex.getMessage();
					LogUtils.d(TAG, "Error: " + msg);
				}
				LogUtils.d(TAG, "AsyncTask completed: " + msg);
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				LogUtils.d(TAG, "Registered with GCM Server." + msg);
				mGCMCallback.onGetRegID();
			}
		}.execute(null, null, null);
	}

	private void storeRegistrationId(String regId) {
		int appVersion = getAppVersion(mContext);
		LogUtils.d(TAG, "Saving regId on app version " + appVersion);

		mPrefsManager.setDeviceID(regId);
		mPrefsManager.setAppVersion(appVersion);
	}
}
