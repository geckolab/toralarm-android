
package com.eisterhues_media_2.singleton;

import android.graphics.drawable.Drawable;

public class Global {
	private static Global mInstance = null;
	private Drawable mBackground = null;
	private boolean mStart = false;

	protected Global() {
	}

	public static Global getInstance() {
		if (mInstance == null) {
			mInstance = new Global();
		}
		return mInstance;
	}

	public Drawable getBackground() {
		return mBackground;
	}

	public void setBackground(Drawable background) {
		mBackground = background;
	}

	public boolean isStart() {
		return mStart;
	}

	public void setStart(boolean start) {
		mStart = start;
	}

}
