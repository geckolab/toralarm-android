
package com.eisterhues_media_2;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.InjectView;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;

public class ImpessumActivity extends BaseActivity {

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@InjectView(R.id.impressum)
	TextView mImpressum;

	@Override
	public int layout() {
		return R.layout.activity_impessum;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getTitleView().setText(getString(R.string.impessum));

		getBackButton().setVisibility(View.VISIBLE);
		getBackButton().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		mImpressum
				.setText(Html
						.fromHtml("<p style=\"color:white;text-align:left;font-size:12px;text-decoration: underline;font-family: 'HelveticaNeue-Light';\">Verantwortlich im Sinne des Telemediengesetzes (TMG):</p><p style=\"color:white;text-align:left;font-size:12px;font-family: 'HelveticaNeue-Light';\"><b>TorAlarm GmbH</b><br />D�sseldorfer Stra�e 27<br />40878 Ratingen<br />Deutschland</p><p style=\"color:white;text-align:left;font-size:12px;text-decoration: underline;font-family: 'HelveticaNeue-Light';\">E-Mail-Adresse:</p><p style=\"color:white;text-align:left;font-size:12px;font-family: 'HelveticaNeue-Light';\">info@toralarm.eu</p><p style=\"color:white;text-align:left;font-size:12px;text-decoration: underline;font-family: 'HelveticaNeue-Light';\">Registergericht:</p><p style=\"color:white;text-align:left;font-size:12px;font-family: 'HelveticaNeue-Light';\">Amtsgericht D�sseldorf</p><p style=\"color:white;text-align:left;font-size:12px;text-decoration: underline;font-family: 'HelveticaNeue-Light';\">Handelsregisternummer:</p><p style=\"color:white;text-align:left;font-size:12px;font-family: 'HelveticaNeue-Light';\">D�sseldorf HRB 70338</p><p style=\"color:white;text-align:left;font-size:12px;text-decoration: underline;font-family: 'HelveticaNeue-Light';\">UST-ID:</p><p style=\"color:white;text-align:left;font-size:12px;font-family: 'HelveticaNeue-Light';\">DE815456584</p><p style=\"color:white;text-align:left;font-size:12px;text-decoration: underline;font-family: 'HelveticaNeue-Light';\">Vertretungsberechtigte Gesch�ftsf�hrer:</p><p style=\"color:white;text-align:left;font-size:12px;font-family: 'HelveticaNeue-Light';\">Maurice Eisterhues<br />Marcel Eisterhues</p><p style=\"color:white;text-align:left;font-size:12px;text-decoration: underline;font-family: 'HelveticaNeue-Light';\">Gesellschafter:</p><p style=\"color:white;text-align:left;font-size:12px;font-family: 'HelveticaNeue-Light';\">Dirk Eisterhues<br />Marcel Eisterhues<br />Maurice Eisterhues<br />The AppHouseCompany GmbH</p><p style=\"color:white;text-align:left;font-size:12px;text-decoration: underline;font-family: 'HelveticaNeue-Light';\">Haftung:</p><p style=\"color:white;text-align:left;font-size:12px;font-family: 'HelveticaNeue-Light';\">Die TorAlarm GmbH �bernimmt keine Haftung oder Garantie f�r die Aktualit�t, Korrektheit und Vollst�ndigkeit der zur Verf�gung gestellten Informationen.</p>"));

	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}

}
