package com.eisterhues_media_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import butterknife.InjectView;
import butterknife.Optional;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.fragment.CompetitionFragment;
import com.eisterhues_media_2.fragment.DetailsFragment;
import com.eisterhues_media_2.fragment.TableFragment;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;

public class CompetitionFragmentActivity extends BaseActivity {

	private int mSelectedRound;
	private String mCompetition;
	private TableFragment tableFragment;
	private CompetitionFragment competitionFragment;
	private DetailsFragment detailsFragment;

	private MyPageAdapter pageAdapter;

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@Optional
	@InjectView(R.id.fragment_left)
	ViewGroup mLeft;

	@Optional
	@InjectView(R.id.viewpager)
	ViewPager mViewPager;

	@Optional
	@InjectView(R.id.fragment_right)
	ViewGroup mRight;

	@Optional
	@InjectView(R.id.swipe_container)
	SwipeRefreshLayout mRefresh;

	@Override
	public int layout() {
		return R.layout.activity_competition_fragment;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (mRefresh != null) {
			mRefresh.setOnRefreshListener(new OnRefreshListener() {

				@Override
				public void onRefresh() {
					mRefresh.setOnRefreshListener(this);
					mRefresh.setColorScheme(android.R.color.white,
							android.R.color.holo_green_light,
							android.R.color.white,
							android.R.color.holo_green_light);

					refresh();
				}

			});
		}

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		mCompetition = getIntent().getExtras()
				.getString(Const.COMPETITION_TYPE);
		mSelectedRound = getIntent().getExtras().getInt(Const.SELECTED_ROUND,
				-1);

		getMenuButton().setVisibility(View.VISIBLE);
		getMenuButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(CompetitionFragmentActivity.this,
						MainActivity.class);
				startActivity(i);
			}
		});

		getReloadButton().setVisibility(View.VISIBLE);
		getReloadButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				refresh();
			}
		});

		if (savedInstanceState != null) {

		} else {
			if (mLeft != null) {
				competitionFragment = new CompetitionFragment(mCompetition,
						mSelectedRound);
				FragmentTransaction fragmentTransaction = getSupportFragmentManager()
						.beginTransaction();
				fragmentTransaction.replace(mLeft.getId(), competitionFragment,
						CompetitionFragment.class.getName());

				fragmentTransaction.commit();
			}

			if (mRight != null && !mCompetition.equals(Const.POKAL)
					&& !mCompetition.equals(Const.LAND)) {
				tableFragment = new TableFragment(mCompetition, mSelectedRound);
				FragmentTransaction fragmentTransaction = getSupportFragmentManager()
						.beginTransaction();
				fragmentTransaction.replace(mRight.getId(), tableFragment,
						TableFragment.class.getName());

				fragmentTransaction.commit();
			} else if (mRight != null || mCompetition.equals(Const.POKAL)
					&& mCompetition.equals(Const.LAND))
				mRight.setEnabled(false);

			if (mViewPager != null) {
				pageAdapter = new MyPageAdapter(getSupportFragmentManager());
				mViewPager.setAdapter(pageAdapter);
			}
		}
	}

	public void competition() {
		if (mViewPager != null) {
			mViewPager.setCurrentItem(0);
		}
	}

	public void table() {
		if (mViewPager != null) {
			mViewPager.setCurrentItem(1);
		}
	}

	public void details(final String matchID) {
		if (mLeft != null) {
			detailsFragment = new DetailsFragment(mCompetition, matchID,
					mSelectedRound);
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(mLeft.getId(), detailsFragment,
					DetailsFragment.class.getName());

			fragmentTransaction.commit();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}

	private void refresh() {
		competitionFragment.getData(true);

		if (mRight.isEnabled())
			tableFragment.getData(true);

		if (mRefresh != null)
			mRefresh.setRefreshing(false);
	}

	public ViewGroup getLeft() {
		return mLeft;
	}

	public void setLeft(ViewGroup left) {
		mLeft = left;
	}

	public ViewGroup getRight() {
		return mRight;
	}

	public void setRight(ViewGroup right) {
		mRight = right;
	}

	class MyPageAdapter extends FragmentPagerAdapter {

		public MyPageAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				return new CompetitionFragment(mCompetition, mSelectedRound);
			case 1:
				return new TableFragment(mCompetition, mSelectedRound);
			}
			return null;
		}

		@Override
		public int getCount() {
			return 2;
		}
	}

}
