
package com.eisterhues_media_2.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefsManager {
	private Context mContext;
	private SharedPreferences mPrefs;

	public PrefsManager(Context context) {
		mContext = context;
		mPrefs = mContext.getSharedPreferences(Const.PREF_NAME,
				Context.MODE_PRIVATE);
	}

	public SharedPreferences getPrefs() {
		return mPrefs;
	}

	public void setPrefs(SharedPreferences prefs) {
		mPrefs = prefs;
	}

	public boolean isBackgroundSet() {
		if (mPrefs.getString(Const.BACKGROUND_NAME,
				Const.BACKGROUND_NAME_DEFAULT_VALUE).equals(
				Const.BACKGROUND_NAME_DEFAULT_VALUE))
			return false;
		return true;
	}

	public boolean setPushNotificationEnable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.PUSH_NOTIFICATION_ENABLE, enable)
				.commit();
	}

	public boolean isPushNotificationEnable() {
		return mPrefs.getBoolean(Const.PUSH_NOTIFICATION_ENABLE,
				Const.PUSH_NOTIFICATION_ENABLE_DEFAULT_VALUE);
	}

	public boolean setPushVibEnable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.PUSH_VIB, enable)
				.commit();
	}

	public boolean isPushVibEnable() {
		return mPrefs.getBoolean(Const.PUSH_VIB,
				Const.PUSH_VIB_DEFAULT_VALUE);
	}

	public boolean setPushMuteEnable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.PUSH_MUTE, enable)
				.commit();
	}

	public boolean isPushMuteEnable() {
		return mPrefs.getBoolean(Const.PUSH_MUTE,
				Const.PUSH_MUTE_DEFAULT_VALUE);
	}

	public boolean setAATKitEnable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.AATKIT_ENABLE, enable)
				.commit();
	}

	public boolean isAATKitEnable() {
		return mPrefs.getBoolean(Const.AATKIT_ENABLE,
				Const.AATKIT_ENABLE_DEFAULT_VALUE);
	}

	public boolean setNotyficationSound(int sound) {
		return mPrefs.edit().putInt(Const.NOTYFICATION_SOUND, sound).commit();
	}

	public int getNotyficationSound() {
		return mPrefs
				.getInt(Const.NOTYFICATION_SOUND, Const.NOTYFICATION_SOUND_DEFAULT_VALUE);
	}

	public boolean setBackground(int width, int height) {
		return mPrefs.edit()
				.putString(Const.BACKGROUND_NAME, height + "x" + width)
				.commit();
	}

	public String getBackground() {
		return mPrefs.getString(Const.BACKGROUND_NAME,
				Const.BACKGROUND_NAME_DEFAULT_VALUE);
	}

	public boolean setDeviceID(String deviceID) {
		return mPrefs.edit().putString(Const.DEVICE_ID, deviceID).commit();
	}

	public String getDeviceID() {
		return mPrefs.getString(Const.DEVICE_ID, Const.DEVICE_ID_DEFAULT_VALUE);
	}

	public boolean setAppVersion(int appVersion) {
		return mPrefs.edit().putInt(Const.APP_VERSION, appVersion).commit();
	}

	public int getAppVersion() {
		return mPrefs
				.getInt(Const.APP_VERSION, Const.APP_VERSION_DEFAULT_VALUE);
	}

	public boolean setUserID(String userID) {
		return mPrefs.edit().putString(Const.USER_ID, userID).commit();
	}

	public String getUserID() {
		return mPrefs.getString(Const.USER_ID, Const.USER_ID_DEFAULT_VALUE);
	}

	public boolean setLanguage(String lng) {
		return mPrefs.edit().putString(Const.LANGUAGE, lng).commit();
	}

	public String getLanguage() {
		return mPrefs
				.getString(Const.LANGUAGE, Const.LANGUAGE_DEFAULT_VALUE);
	}

	public boolean setLiga1Enable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.LIGA1, enable)
				.commit();
	}

	public boolean isLiga1Enable() {
		return mPrefs.getBoolean(Const.LIGA1,
				true);
	}

	public boolean setLiga2Enable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.LIGA2, enable)
				.commit();
	}

	public boolean isLiga2Enable() {
		return mPrefs.getBoolean(Const.LIGA2,
				true);
	}

	public boolean setLiga3Enable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.LIGA3, enable)
				.commit();
	}

	public boolean isLiga3Enable() {
		return mPrefs.getBoolean(Const.LIGA3,
				true);
	}

	public boolean setPokalEnable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.POKAL, enable)
				.commit();
	}

	public boolean isPokalEnable() {
		return mPrefs.getBoolean(Const.POKAL,
				true);
	}

	public boolean setELEnable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.EL, enable)
				.commit();
	}

	public boolean isELEnable() {
		return mPrefs.getBoolean(Const.EL,
				true);
	}

	public boolean setCLEnable(boolean enable) {
		return mPrefs.edit().putBoolean(Const.CL, enable)
				.commit();
	}

	public boolean isCLEnable() {
		return mPrefs.getBoolean(Const.CL,
				true);
	}

	public boolean setPush(String key, int value) {
		return mPrefs.edit().putInt(key, value)
				.commit();
	}

	public int getPush(String key, int def) {
		return mPrefs.getInt(key, def);
	}

	public boolean setLastAction(int action) {
		return mPrefs.edit().putInt(Const.LAST_ACTION, action).commit();
	}

	public int getLastAction() {
		return mPrefs
				.getInt(Const.LAST_ACTION, Const.LAST_ACTION_DEFAULT_VALUE);
	}

	public boolean setAppStarts(int starts) {
		return mPrefs.edit().putInt(Const.APP_STARTS, starts).commit();
	}

	public int getAppStarts() {
		return mPrefs
				.getInt(Const.APP_STARTS, Const.APP_STARTS_DEFAULT_VALUE);
	}

	public boolean setMain(int i, int value) {
		return mPrefs.edit().putInt("main-" + i, value)
				.commit();
	}

	public int getMain(int i) {
		return mPrefs.getInt("main-" + i, i);
	}
}
