
package com.eisterhues_media_2.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

import com.eisterhues_media_2.R;
import com.eisterhues_media_2.singleton.Global;
import com.intentsoftware.addapptr.AATKit;

public class Utils {

	private static int[] mHeightArray = {
			320, 400, 480, 640, 800, 854, 960,
			1024, 1280, 1440, 1920, 2048, 2560
	};
	private static int[] mWidthArray = {
			240, 320, 360, 480, 540, 600, 640,
			720, 768, 800, 900, 1080, 1200, 1280, 1536, 1600
	};

	@SuppressWarnings("deprecation")
	public static void loadBackground(PrefsManager prefsManager, Activity a,
			View v) {
		a.overridePendingTransition(0, 0);
		if (Global.getInstance().getBackground() != null) {
			v.setBackgroundDrawable(Global.getInstance().getBackground());
		} else {
			if (prefsManager.isBackgroundSet()) {
				try {
					Global.getInstance().setBackground(
							Drawable.createFromStream(
									a.getAssets().open(
											prefsManager.getBackground()
													+ "/bg.png"), null));

					v.setBackgroundDrawable(Global.getInstance()
							.getBackground());
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				DisplayMetrics metrics = new DisplayMetrics();
				a.getWindowManager().getDefaultDisplay().getMetrics(metrics);

				int width = findNearestNumber(mWidthArray, metrics.widthPixels);
				int height = findNearestNumber(mHeightArray,
						metrics.heightPixels);

				prefsManager.setBackground(width, height);

				try {
					Global.getInstance().setBackground(
							Drawable.createFromStream(
									a.getAssets().open(
											prefsManager.getBackground()
													+ "/bg.png"), null));

					v.setBackgroundDrawable(Global.getInstance()
							.getBackground());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static int findNearestNumber(int[] array, int number) {
		int distance = Math.abs(array[0] - number);
		int idx = 0;
		for (int c = 1; c < array.length; c++) {
			int cdistance = Math.abs(array[c] - number);
			if (cdistance < distance) {
				idx = c;
				distance = cdistance;
			}
		}
		return array[idx];
	}

	public static String getDate(String timestamp) {
		long time;
		time = Long.parseLong(timestamp);
		Calendar cal = Calendar.getInstance();
		TimeZone tz = cal.getTimeZone();// get your local time zone.
		SimpleDateFormat sdf = new SimpleDateFormat("EE dd.MM.yyyy HH:mm");
		sdf.setTimeZone(tz);// set time zone.
		String localTime = sdf.format(new Date(time * 1000));
		Date date = new Date();
		try {
			date = sdf.parse(localTime);// get local date
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date.toString();
	}

	public static String getIconURL(String team) {
		team = team.toLowerCase(Locale.GERMAN);

		team = team.replaceAll("1", "");
		team = team.replaceAll("2", "");
		team = team.replaceAll("3", "");
		team = team.replaceAll("4", "");
		team = team.replaceAll("5", "");
		team = team.replaceAll("6", "");
		team = team.replaceAll("7", "");
		team = team.replaceAll("8", "");
		team = team.replaceAll("9", "");
		team = team.replaceAll("0", "");
		team = team.replaceAll(" ", "");
		team = team.replaceAll("ä", "ae");
		team = team.replaceAll("ö", "oe");
		team = team.replaceAll("ü", "ue");
		team = team.replaceAll("ß", "ss");

		return Const.API_ICON_DOWNLOAD + "/" + team + ".png";
	}

	public static int isTablet(Context context) {
		TelephonyManager manager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		if (manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
			return 1;
		} else {
			return 0;
		}
	}

	public static String decode(String base64) {
		byte[] data = Base64.decode(base64, Base64.DEFAULT);
		String outStr = "";
		try {
			GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(
					data));
			BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));

			String line;
			while ((line = bf.readLine()) != null) {
				outStr += line;
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return outStr;
	}

	public static void play(Context c, int which) {

		if (which == 0) {
			Vibrator v = (Vibrator) c.getSystemService(Context.VIBRATOR_SERVICE);
			v.vibrate(500);
		}
		if (which == 1) {
			MediaPlayer mp = MediaPlayer.create(c, R.raw.whistle);
			mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer arg0) {
					arg0.release();
				}
			});
		}
		if (which == 2) {
			MediaPlayer mp = MediaPlayer.create(c, R.raw.honk);
			mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer arg0) {
					arg0.release();
				}
			});
		}
		if (which == 3) {
			MediaPlayer mp = MediaPlayer.create(c, R.raw.jingle);
			mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer arg0) {
					arg0.release();
				}
			});
		}
		if (which == 4) {
			MediaPlayer mp = MediaPlayer.create(c, R.raw.cheer);
			mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer arg0) {
					arg0.release();
				}
			});
		}

	}

	public static void addPlacementView(final View v, int placementId) {
		View placementView = AATKit.getPlacementView(placementId);
		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		layoutParams.gravity = Gravity.CENTER | Gravity.BOTTOM;
		((ViewGroup) v).addView(placementView, layoutParams);
	}

	public static void removePlacementView(int placementId) {
		View placementView = AATKit.getPlacementView(placementId);

		if (placementView.getParent() != null) {
			ViewGroup parent = (ViewGroup) placementView.getParent();
			parent.removeView(placementView);
		}
	}
}
