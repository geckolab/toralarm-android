
package com.eisterhues_media_2.utils;

import android.util.Log;

import com.eisterhues_media_2.BuildConfig;

public class LogUtils {
	public static void d(final String tag, String message) {
		if (BuildConfig.DEBUG || Const.ALLOW_DEBUG) {
			Log.d(tag, message);
		}
	}

	public static void v(final String tag, String message) {
		if (BuildConfig.DEBUG || Const.ALLOW_DEBUG) {
			Log.v(tag, message);
		}
	}

	public static void i(final String tag, String message) {
		if (BuildConfig.DEBUG || Const.ALLOW_DEBUG) {
			Log.i(tag, message);
		}
	}

	public static void w(final String tag, String message) {
		if (BuildConfig.DEBUG || Const.ALLOW_DEBUG) {
			Log.w(tag, message);
		}
	}

	public static void e(final String tag, String message) {
		if (BuildConfig.DEBUG || Const.ALLOW_DEBUG) {
			Log.e(tag, message);
		}
	}
}
