
package com.eisterhues_media_2.utils;

public class Const {
	public static final boolean ALLOW_DEBUG = true;
	public static boolean AATKIT = true;
	public static int APP_ICON;

	public static final String GOOGLE_PROJECT_ID = "672560419447";
	public static final String MESSAGE_KEY = "message";
	public static final int DEVICE_TYPE = 1;
	public static final int MAIN_MENU_ICONS = 10;

	// prefs keys&defaults
	public static final String PREF_NAME = "com.eisterhues_media_2.PREFS";

	public static final String BACKGROUND_NAME = "com.eisterhues_media_2.BACKGROUND_NAME";
	public static final String BACKGROUND_NAME_DEFAULT_VALUE = "com.eisterhues_media_2.BACKGROUND_DEFAULT_VALUE";

	public static final String PUSH_NOTIFICATION_ENABLE = "com.eisterhues_media_2.PUSH_NOTIFICATION_ENABLE";
	public static final boolean PUSH_NOTIFICATION_ENABLE_DEFAULT_VALUE = true;

	public static final String DEVICE_ID = "com.eisterhues_media_2.DEVICE_ID";
	public static final String DEVICE_ID_DEFAULT_VALUE = "";

	public static final String APP_VERSION = "com.eisterhues_media_2.APP_VERSION";
	public static final int APP_VERSION_DEFAULT_VALUE = Integer.MIN_VALUE;
	
	public static final String USER_ID = "com.litegames.soccerquiz.USER_ID";
	public static final String USER_ID_DEFAULT_VALUE = "";

	public static final String LANGUAGE = "com.eisterhues_media_2.LANGUAGE";
	public static final String LANGUAGE_DEFAULT_VALUE = "de";
	
	public static final String LAST_ACTION = "com.eisterhues_media_2.LAST_ACTION";
	public static final int LAST_ACTION_DEFAULT_VALUE = 0;
	
	public static final String APP_STARTS = "com.eisterhues_media_2.APP_STARTS";
	public static final int APP_STARTS_DEFAULT_VALUE = 0;
	
	public static final String NOTYFICATION_SOUND = "com.eisterhues_media_2.NOTYFICATION_SOUND";
	public static final int NOTYFICATION_SOUND_DEFAULT_VALUE = 1;
	
	public static final String PUSH_VIB = "com.eisterhues_media_2.PUSH_VIB";
	public static final boolean PUSH_VIB_DEFAULT_VALUE = true;
	
	public static final String PUSH_MUTE = "com.eisterhues_media_2.PUSH_MUTE";
	public static final boolean PUSH_MUTE_DEFAULT_VALUE = false;
	
	public static final String AATKIT_ENABLE = "com.eisterhues_media_2.AATKIT_ENABLE";
	public static final boolean AATKIT_ENABLE_DEFAULT_VALUE = true;

	// API
	public static final String API_URL = "http://www.eisterhues-media.com/TA3/json/index.php";
	public static final String API_SETTINGS_URL = "http://www.eisterhues-media.com/TA3/addToken.php";
	public static final String API_BUG_REPORT_URL = "http://www.eisterhues-media.com/TA3/sendBugReport.php";
	public static final String API_ICON_DOWNLOAD = "http://www.eisterhues-media.com/TA3/images";

	public static final String FACEBOOK = "http://www.facebook.com/260637887331743";

	public static final String ADD_REQUEST = "com.eisterhues_media_2.ADD_REQUEST";
	public static final String COMPETITION_TYPE = "com.eisterhues_media_2.COMPETITION_TYPE";
	public static final String SELECTED_ROUND = "com.eisterhues_media_2.SELECTED_ROUND";
	public static final String MATCH_ID = "com.eisterhues_media_2.MATCH_ID";
	public static final String PUSH_ID = "com.eisterhues_media_2.PUSH_ID";

	public static final int PUSH_DE = 1;
	public static final int PUSH_DE_NUMBER = 57;
	public static final String PUSH_DE_DEFAULT = "[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,￼1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]";

	public static final String LIGA1 = "com.eisterhues_media_2.LIGA1";
	public static final String LIGA2 = "com.eisterhues_media_2.LIGA2";
	public static final String LIGA3 = "com.eisterhues_media_2.LIGA3";
	public static final String EL = "com.eisterhues_media_2.EL";
	public static final String CL = "com.eisterhues_media_2.CL";
	public static final String POKAL = "com.eisterhues_media_2.POKAL";
	public static final String LAND = "com.eisterhues_media_2.LAND";
}
