
package com.eisterhues_media_2.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisterhues_media_2.R;

public class MainIcon {

	private View mCustomView;
	private TextView mTitleTextView;
	private ImageView mImageView;

	public MainIcon(Context c, int id, int resID, String text) {
		mCustomView = LayoutInflater.from(c).inflate(R.layout.main_menu_icon, null);
		mCustomView.setTag(Integer.valueOf(id));

		mTitleTextView = (TextView) mCustomView.findViewById(R.id.text);
		mTitleTextView.setText(text);

		mImageView = (ImageView) mCustomView.findViewById(R.id.icon);
		mImageView.setImageDrawable(c.getResources().getDrawable(resID));
	}

	public View getCustomView() {
		return mCustomView;
	}

	public void setCustomView(View customView) {
		mCustomView = customView;
	}

}
