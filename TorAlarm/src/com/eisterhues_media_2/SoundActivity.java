
package com.eisterhues_media_2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import butterknife.InjectView;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;

public class SoundActivity extends BaseActivity {

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@InjectView(R.id.check_mute)
	CheckBox mMute;

	@InjectView(R.id.check_vib)
	CheckBox mVib;

	@InjectView(R.id.check_default)
	CheckBox mDefault;

	@InjectView(R.id.check_whistle)
	CheckBox mWhistle;

	@InjectView(R.id.check_honk)
	CheckBox mHonk;

	@InjectView(R.id.check_jingle)
	CheckBox mJingle;

	@InjectView(R.id.check_cheer)
	CheckBox mCheer;

	@Override
	public int layout() {
		return R.layout.activity_sound;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getTitleView().setText(getString(R.string.sound));

		getBackButton().setVisibility(View.VISIBLE);
		getBackButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(SoundActivity.this,
						SettingsActivity.class);
				startActivity(i);
			}
		});

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		if (getPrefsManager().isPushMuteEnable())
			mMute.setChecked(true);
		mMute
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						getPrefsManager().setPushMuteEnable(isChecked);
					}
				});

		if (getPrefsManager().isPushVibEnable())
			mVib.setChecked(true);
		mVib
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						getPrefsManager().setPushVibEnable(isChecked);
					}
				});

		if (getPrefsManager().getNotyficationSound() == 0)
			mDefault.setChecked(true);
		mDefault
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							getPrefsManager().setNotyficationSound(0);
							mHonk.setChecked(false);
							mWhistle.setChecked(false);
							mJingle.setChecked(false);
							mCheer.setChecked(false);

							Utils.play(SoundActivity.this, 0);
						}
					}
				});

		if (getPrefsManager().getNotyficationSound() == 1)
			mWhistle.setChecked(true);
		mWhistle
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							getPrefsManager().setNotyficationSound(1);
							mDefault.setChecked(false);
							mHonk.setChecked(false);
							mJingle.setChecked(false);
							mCheer.setChecked(false);

							Utils.play(SoundActivity.this, 1);
						}
					}
				});

		if (getPrefsManager().getNotyficationSound() == 2)
			mHonk.setChecked(true);
		mHonk
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							getPrefsManager().setNotyficationSound(2);
							mDefault.setChecked(false);
							mWhistle.setChecked(false);
							mJingle.setChecked(false);
							mCheer.setChecked(false);

							Utils.play(SoundActivity.this, 2);
						}
					}
				});

		if (getPrefsManager().getNotyficationSound() == 3)
			mJingle.setChecked(true);
		mJingle
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							getPrefsManager().setNotyficationSound(3);
							mDefault.setChecked(false);
							mHonk.setChecked(false);
							mWhistle.setChecked(false);
							mCheer.setChecked(false);

							Utils.play(SoundActivity.this, 3);
						}
					}
				});

		if (getPrefsManager().getNotyficationSound() == 4)
			mCheer.setChecked(true);
		mCheer
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							getPrefsManager().setNotyficationSound(4);
							mDefault.setChecked(false);
							mHonk.setChecked(false);
							mWhistle.setChecked(false);
							mJingle.setChecked(false);

							Utils.play(SoundActivity.this, 4);
						}
					}
				});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}
}
