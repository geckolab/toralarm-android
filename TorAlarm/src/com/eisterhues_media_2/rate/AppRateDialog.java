
package com.eisterhues_media_2.rate;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisterhues_media_2.R;

public class AppRateDialog {

	public static void app_launched(Context mContext) {
		SharedPreferences prefs = mContext.getSharedPreferences("AppRateDialog", 0);
		if (prefs.getBoolean("DontShowAgain", false)) {
			return;
		}

		SharedPreferences.Editor editor = prefs.edit();

		// Increment launch counter
		long launchCount = prefs.getLong("LaunchCount", 0) + 1;
		editor.putLong("LaunchCount", launchCount);

		// Wait at least n days before opening
		if (launchCount == 3 || launchCount == 8 || launchCount == 15) {
			showRateDialog(mContext, editor);
		}

		editor.commit();
	}

	public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(dialog.getWindow().FEATURE_NO_TITLE); 

		LinearLayout ll = new LinearLayout(mContext);
		ll.setOrientation(LinearLayout.VERTICAL);

		TextView tv = new TextView(mContext);
		tv.setText(mContext.getResources().getString(R.string.dialog_rateMessage));
		tv.setWidth(240);
		tv.setGravity(Gravity.CENTER_HORIZONTAL);
		tv.setPadding(4, 0, 4, 10);
		ll.addView(tv);

		Button b1 = new Button(mContext);
		b1.setText(mContext.getResources().getString(R.string.dialog_buttonRateNow));
		b1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mContext.getPackageName())));
				editor.putBoolean("DontShowAgain", true);
				editor.commit();
				dialog.dismiss();
			}
		});
		ll.addView(b1);

		Button b2 = new Button(mContext);
		b2.setText(mContext.getResources().getString(R.string.dialog_buttonRateLater));
		b2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		ll.addView(b2);

		dialog.setContentView(ll);
		dialog.show();
	}
}
