package com.eisterhues_media_2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.AccountManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.InjectView;

import com.animoto.android.views.DraggableGridView;
import com.animoto.android.views.OnRearrangeListener;
import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.gcm.GCM;
import com.eisterhues_media_2.gcm.GCMCallback;
import com.eisterhues_media_2.network.SettingsNetworkHelper;
import com.eisterhues_media_2.network.SettingsNetworkHelper.SettingsNetworkHelperInterface;
import com.eisterhues_media_2.network.input.InputParams;
import com.eisterhues_media_2.network.response.AddToken;
import com.eisterhues_media_2.singleton.Global;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.LogUtils;
import com.eisterhues_media_2.utils.MainIcon;
import com.eisterhues_media_2.utils.Utils;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;
import com.intentsoftware.addapptr.AATKit;

public class MainActivity extends BaseActivity {

	private static final int REQUEST_CODE_EMAIL = 1;
	private GCM mGCM;
	private SettingsNetworkHelper mNetworkHelper;

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@InjectView(R.id.dgv)
	DraggableGridView dgv;

	@Override
	public int layout() {
		return R.layout.activity_main;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getTitleView().setText(getString(R.string.app_name));

		getSettingsButton().setVisibility(View.VISIBLE);
		getSettingsButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				// Intent i = new Intent(MainActivity.this,
				// SettingsActivity.class);
				Intent i = new Intent(MainActivity.this,
						SettingsFragmentActivity.class);
				i.putExtra(Const.ADD_REQUEST, false);
				startActivity(i);
			}
		});

		getPrefsManager().setAppStarts(getPrefsManager().getAppStarts() + 1);
		Utils.loadBackground(getPrefsManager(), this, mBackground);

		dgv.setOnRearrangeListener(new OnRearrangeListener() {
			public void onRearrange(int oldIndex, int newIndex) {
				save();
			}
		});

		dgv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if (arg1 != null) {
					if ((Integer) arg1.getTag() == 1 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_1liga_1));
						// Intent i = new Intent(MainActivity.this,
						// CompetitionActivity.class);
						Intent i = new Intent(MainActivity.this,
								CompetitionFragmentActivity.class);
						i.putExtra(Const.COMPETITION_TYPE, Const.LIGA1);
						startActivity(i);
					} else if ((Integer) arg1.getTag() == 1)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_1liga_0));

					if ((Integer) arg1.getTag() == 2 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_2liga_1));
						// Intent i = new Intent(MainActivity.this,
						// CompetitionActivity.class);
						Intent i = new Intent(MainActivity.this,
								CompetitionFragmentActivity.class);
						i.putExtra(Const.COMPETITION_TYPE, Const.LIGA2);
						startActivity(i);
					} else if ((Integer) arg1.getTag() == 2)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_2liga_0));

					if ((Integer) arg1.getTag() == 3 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_3liga_1));
						// Intent i = new Intent(MainActivity.this,
						// CompetitionActivity.class);
						Intent i = new Intent(MainActivity.this,
								CompetitionFragmentActivity.class);
						i.putExtra(Const.COMPETITION_TYPE, Const.LIGA3);
						startActivity(i);
					} else if ((Integer) arg1.getTag() == 3)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_3liga_0));

					if ((Integer) arg1.getTag() == 4 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_cl_1));
						// Intent i = new Intent(MainActivity.this,
						// CompetitionActivity.class);
						Intent i = new Intent(MainActivity.this,
								CompetitionFragmentActivity.class);
						i.putExtra(Const.COMPETITION_TYPE, Const.CL);
						startActivity(i);
					} else if ((Integer) arg1.getTag() == 4)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_cl_0));

					if ((Integer) arg1.getTag() == 5 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_el_1));
						// Intent i = new Intent(MainActivity.this,
						// CompetitionActivity.class);
						Intent i = new Intent(MainActivity.this,
								CompetitionFragmentActivity.class);
						i.putExtra(Const.COMPETITION_TYPE, Const.EL);
						startActivity(i);
					} else if ((Integer) arg1.getTag() == 5)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_cl_0));

					if ((Integer) arg1.getTag() == 6 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_pokal_1));
						// Intent i = new Intent(MainActivity.this,
						// CompetitionActivity.class);
						Intent i = new Intent(MainActivity.this,
								CompetitionFragmentActivity.class);
						i.putExtra(Const.COMPETITION_TYPE, Const.POKAL);
						startActivity(i);
					} else if ((Integer) arg1.getTag() == 6)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_pokal_0));

					if ((Integer) arg1.getTag() == 7 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_land_1));
						// Intent i = new Intent(MainActivity.this,
						// CompetitionActivity.class);
						Intent i = new Intent(MainActivity.this,
								CompetitionFragmentActivity.class);
						i.putExtra(Const.COMPETITION_TYPE, Const.LAND);
						startActivity(i);
					} else if ((Integer) arg1.getTag() == 7)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_land_1));

					if ((Integer) arg1.getTag() == 8 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_rate_1));
						Uri uri = Uri.parse("market://details?id="
								+ getPackageName());
						Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
						try {
							startActivity(goToMarket);
						} catch (ActivityNotFoundException e) {
							startActivity(new Intent(
									Intent.ACTION_VIEW,
									Uri.parse("http://play.google.com/store/apps/details?id="
											+ getPackageName())));
						}
					} else if ((Integer) arg1.getTag() == 8)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_rate_0));

					if ((Integer) arg1.getTag() == 9 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_fb_1));
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri
								.parse(Const.FACEBOOK));
						startActivity(intent);
					} else if ((Integer) arg1.getTag() == 9)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_fb_0));

					if ((Integer) arg1.getTag() == 10 && arg2 != -1) {
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_adfree_1));
						// Intent i = new Intent(MainActivity.this,
						// AddFreeActivity.class);
						Intent i = new Intent(MainActivity.this,
								SettingsFragmentActivity.class);
						i.putExtra(Const.ADD_REQUEST, true);
						startActivity(i);
					} else if ((Integer) arg1.getTag() == 10)
						((ImageView) arg1.findViewById(R.id.icon))
								.setImageDrawable(getResources().getDrawable(
										R.drawable.mmicon_adfree_0));
				}
			}
		});

		initMainMenu();

		mGCM = new GCM(this, getPrefsManager(), new GCMCallback() {

			@Override
			public void onGetRegID() {
				gerUserID();
			}
		});
		if (!Global.getInstance().isStart())
			mGCM.registerGCM();
	}

	private void initMainMenu() {
		dgv.removeAllViews();
		for (int i = 1; i <= Const.MAIN_MENU_ICONS; i++) {
			if (getPrefsManager().getMain(i) == 1
					&& getPrefsManager().isLiga1Enable()) {
				MainIcon mi1 = new MainIcon(this, 1, R.drawable.mmicon_1liga_0,
						getString(R.string.liga1));
				dgv.addView(mi1.getCustomView());
			}

			if (getPrefsManager().getMain(i) == 2
					&& getPrefsManager().isLiga2Enable()) {
				MainIcon mi2 = new MainIcon(this, 2, R.drawable.mmicon_2liga_0,
						getString(R.string.liga2));
				dgv.addView(mi2.getCustomView());
			}

			if (getPrefsManager().getMain(i) == 3
					&& getPrefsManager().isLiga3Enable()) {
				MainIcon mi3 = new MainIcon(this, 3, R.drawable.mmicon_3liga_0,
						getString(R.string.liga3));
				dgv.addView(mi3.getCustomView());
			}

			if (getPrefsManager().getMain(i) == 4
					&& getPrefsManager().isCLEnable()) {
				MainIcon mi4 = new MainIcon(this, 4, R.drawable.mmicon_cl_0,
						getString(R.string.cl));
				dgv.addView(mi4.getCustomView());
			}

			if (getPrefsManager().getMain(i) == 5
					&& getPrefsManager().isELEnable()) {
				MainIcon mi5 = new MainIcon(this, 5, R.drawable.mmicon_el_0,
						getString(R.string.el));
				dgv.addView(mi5.getCustomView());
			}

			if (getPrefsManager().getMain(i) == 6
					&& getPrefsManager().isPokalEnable()) {
				MainIcon mi6 = new MainIcon(this, 6, R.drawable.mmicon_pokal_0,
						getString(R.string.pokal));
				dgv.addView(mi6.getCustomView());
			}

			if (getPrefsManager().getMain(i) == 7) {
				MainIcon mi7 = new MainIcon(this, 7, R.drawable.mmicon_land_0,
						getString(R.string.land));
				dgv.addView(mi7.getCustomView());
			}

			if (getPrefsManager().getMain(i) == 8) {
				MainIcon mi8 = new MainIcon(this, 8, R.drawable.mmicon_rate_0,
						getString(R.string.rate));
				dgv.addView(mi8.getCustomView());
			}

			if (getPrefsManager().getMain(i) == 9) {
				MainIcon mi9 = new MainIcon(this, 9, R.drawable.mmicon_fb_0,
						getString(R.string.fb));
				dgv.addView(mi9.getCustomView());
			}

			if (getPrefsManager().getMain(i) == 10) {
				MainIcon mi10 = new MainIcon(this, 10,
						R.drawable.mmicon_adfree_0, getString(R.string.add));
				dgv.addView(mi10.getCustomView());
			}
		}
	}

	private void save() {
		int v = 0;
		for (int i = 1; i <= Const.MAIN_MENU_ICONS; i++) {
			if (getPrefsManager().getMain(i) == 1
					&& !getPrefsManager().isLiga1Enable()) {
			} else if (getPrefsManager().getMain(i) == 2
					&& !getPrefsManager().isLiga2Enable()) {
			} else if (getPrefsManager().getMain(i) == 3
					&& !getPrefsManager().isLiga3Enable()) {
			} else if (getPrefsManager().getMain(i) == 4
					&& !getPrefsManager().isCLEnable()) {
			} else if (getPrefsManager().getMain(i) == 5
					&& !getPrefsManager().isELEnable()) {
			} else if (getPrefsManager().getMain(i) == 6
					&& !getPrefsManager().isPokalEnable()) {
			} else {
				View child = dgv.getChildAt(v);
				getPrefsManager().setMain(i, (Integer) child.getTag());
				v++;
			}
		}
	}

	private void gerUserID() {
		if (getPrefsManager().getUserID().equals(Const.USER_ID_DEFAULT_VALUE)) {
			try {
				Intent intent = AccountPicker.newChooseAccountIntent(null,
						null,
						new String[] { GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE },
						false, null, null, null, null);
				startActivityForResult(intent, REQUEST_CODE_EMAIL);
			} catch (ActivityNotFoundException e) {
			}
		} else {
			doLogin();
		}
	}

	@Override
	protected void onActivityResult(final int requestCode,
			final int resultCode, final Intent data) {
		if (requestCode == REQUEST_CODE_EMAIL && resultCode == RESULT_OK) {
			String accountName = data
					.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
			getPrefsManager().setUserID(accountName);
			doLogin();
		}
	}

	private void doLogin() {
		if (!Global.getInstance().isStart()) {
			JSONObject settings = new JSONObject();

			if (!getPrefsManager().getPrefs().contains("1-push")) {
				try {
					LogUtils.d("MODE", "DEF");
					settings.put("0", new JSONArray(Const.PUSH_DE_DEFAULT));
				} catch (JSONException e) {
				}
			} else {
				String arr = "[";
				for (int i = 1; i <= Const.PUSH_DE_NUMBER; i++) {
					arr += getPrefsManager().getPush("1-" + i, 1);

					if (i < Const.PUSH_DE_NUMBER)
						arr += ",";
				}
				arr += "]";

				try {
					LogUtils.d("MODE", "USER");
					settings.put("0", new JSONArray(arr));
				} catch (JSONException e) {
				}
			}

			mNetworkHelper = new SettingsNetworkHelper();
			mNetworkHelper.sentApiRequest(this, InputParams.settings(
					getPrefsManager().getDeviceID(), settings,
					Const.DEVICE_TYPE, getLanguage(), String
							.valueOf(getPrefsManager().getAppVersion()), String
							.valueOf(android.os.Build.VERSION.SDK_INT),
					android.os.Build.MODEL, Utils.isTablet(this),
					getPrefsManager().getLastAction(), getPrefsManager()
							.getAppStarts()),
					new SettingsNetworkHelperInterface() {

						@Override
						public void apiResultAction(JSONArray result) {
							Global.getInstance().setStart(true);

							try {
								AddToken out = new AddToken(result);
							} catch (JSONException e) {
							}
						}
					});
		}
	}

	@Override
	public void onBackPressed() {
		closeAllActivities();
		finish();
		unRegisterBaseActivityReceiver();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}

	@Override
	public void onRestart() {
		super.onRestart();
		initMainMenu();
	}

}
