package com.eisterhues_media_2;

import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.InjectView;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.network.NetworkHelper;
import com.eisterhues_media_2.network.NetworkHelperInterface;
import com.eisterhues_media_2.network.input.InputParams;
import com.eisterhues_media_2.network.response.MatchDetails;
import com.eisterhues_media_2.network.response.MatchDetails.Detail;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.NetworkUtils;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;
import com.squareup.picasso.Picasso;

public class DetailsActivity extends BaseActivity {

	private NetworkHelper mNetworkHelper;
	private LayoutInflater mInflater;
	private View mRoundHeaderView;
	private View mRoundDetailsView;
	private View mDetailsDetailsView;
	private String mMatchID;
	private String mCompetition;
	private int mSelectedRound;

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@InjectView(R.id.linearLayoutMatches)
	LinearLayout mMatches;

	@InjectView(R.id.swipe_container)
	SwipeRefreshLayout mRefresh;

	@InjectView(R.id.quotas)
	LinearLayout mQuotas;

	@InjectView(R.id.bet)
	Button mBet;

	@InjectView(R.id.team1)
	Button mTeam1;

	@InjectView(R.id.teamx)
	Button mTeamX;

	@InjectView(R.id.team2)
	Button mTeam2;

	@Override
	public int layout() {
		return R.layout.activity_details;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mRefresh.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {

				mRefresh.setOnRefreshListener(this);
				mRefresh.setColorScheme(android.R.color.white,
						android.R.color.holo_green_light,
						android.R.color.white, android.R.color.holo_green_light);

				getData(true);
			}

		});

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		mInflater = LayoutInflater.from(this);

		mCompetition = getIntent().getExtras()
				.getString(Const.COMPETITION_TYPE);
		mMatchID = getIntent().getExtras().getString(Const.MATCH_ID);
		mSelectedRound = getIntent().getExtras().getInt(Const.SELECTED_ROUND,
				-1);

		if (mCompetition.equals(Const.LIGA1)) {
			getTitleView().setText(getString(R.string.liga1));
		} else if (mCompetition.equals(Const.LIGA2)) {
			getTitleView().setText(getString(R.string.liga2));
		} else if (mCompetition.equals(Const.LIGA3)) {
			getTitleView().setText(getString(R.string.liga3));
		} else if (mCompetition.equals(Const.CL)) {
			getTitleView().setText(getString(R.string.cl));
		} else if (mCompetition.equals(Const.EL)) {
			getTitleView().setText(getString(R.string.el));
		} else if (mCompetition.equals(Const.POKAL)) {
			getTitleView().setText(getString(R.string.pokal));
		} else if (mCompetition.equals(Const.LAND)) {
			getTitleView().setText(getString(R.string.land));
		}

		getBackButton().setVisibility(View.VISIBLE);
		getBackButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(DetailsActivity.this,
						CompetitionFragmentActivity.class);
				i.putExtra(Const.COMPETITION_TYPE, mCompetition);
				i.putExtra(Const.SELECTED_ROUND, mSelectedRound);
				startActivity(i);
			}
		});

		getReloadButton().setVisibility(View.VISIBLE);
		getReloadButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				getData(true);
			}
		});

		getData(false);
	}

	private void getData(final boolean refresh) {
		if (!NetworkUtils.isNetworkAvailable(this) && refresh)
			Toast.makeText(getApplicationContext(),
					getString(R.string.datanotloaded), Toast.LENGTH_SHORT)
					.show();

		mNetworkHelper = new NetworkHelper();
		mNetworkHelper.sentApiRequest(this, new NetworkHelperInterface() {

			@Override
			public List<BasicNameValuePair> setApiInputParams() {
				return InputParams.matchDetails(Integer.valueOf(mMatchID),
						getLanguage(), 0);
			}

			@Override
			public void apiResultAction(String result) {
				try {
					MatchDetails matchDetails = new MatchDetails(
							new JSONObject(result));

					if (matchDetails.getQuotas() != null) {
						mTeam1.setText(matchDetails.getQuotas().getQuotaTeam1());
						mTeamX.setText(matchDetails.getQuotas().getQuotaX());
						mTeam2.setText(matchDetails.getQuotas().getQuotaTeam2());

						final String url = matchDetails.getQuotas().getUrl();
						mBet.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View arg0) {
								Intent intent = new Intent(Intent.ACTION_VIEW,
										Uri.parse(url));
								startActivity(intent);
							}
						});

						mQuotas.setVisibility(View.VISIBLE);
						mBet.setVisibility(View.VISIBLE);
					}

					mMatches.removeAllViews();

					mRoundHeaderView = mInflater.inflate(R.layout.round_header,
							null);

					TextView left = (TextView) mRoundHeaderView
							.findViewById(R.id.text_left);
					left.setText(Utils
							.getDate(matchDetails.getData().getTime()));

					TextView right = (TextView) mRoundHeaderView
							.findViewById(R.id.text_right);
					right.setText(matchDetails.getData().getRoundText());

					mMatches.addView(mRoundHeaderView);

					mRoundDetailsView = mInflater.inflate(
							R.layout.round_details, null);

					TextView team1 = (TextView) mRoundDetailsView
							.findViewById(R.id.text_left);
					team1.setText(matchDetails.getData().getTeam1());

					TextView score = (TextView) mRoundDetailsView
							.findViewById(R.id.text_score);
					score.setText(matchDetails.getData().getTeam1Goals() + ":"
							+ matchDetails.getData().getTeam2Goals());

					TextView team2 = (TextView) mRoundDetailsView
							.findViewById(R.id.text_right);
					team2.setText(matchDetails.getData().getTeam2());

					ImageView team1Icon = (ImageView) mRoundDetailsView
							.findViewById(R.id.team1_icon);
					Picasso.with(DetailsActivity.this)
							.load(Utils.getIconURL(matchDetails.getData()
									.getTeam1())).into(team1Icon);

					ImageView team2Icon = (ImageView) mRoundDetailsView
							.findViewById(R.id.team2_icon);
					Picasso.with(DetailsActivity.this)
							.load(Utils.getIconURL(matchDetails.getData()
									.getTeam2())).into(team2Icon);

					mMatches.addView(mRoundDetailsView);

					int i = 1;
					for (Detail d : matchDetails.getDetails()) {
						mDetailsDetailsView = mInflater.inflate(
								R.layout.details_details, null);

						if (i % 2 != 0) {
							mDetailsDetailsView.setBackgroundColor(Color
									.parseColor("#000000"));
							mDetailsDetailsView.getBackground().setAlpha(128);
						}

						TextView time = (TextView) mDetailsDetailsView
								.findViewById(R.id.text_time);
						time.setText(d.getTime());

						TextView name = (TextView) mDetailsDetailsView
								.findViewById(R.id.text_name);
						name.setText(d.getPlayerName());

						ImageView teamIcon = (ImageView) mDetailsDetailsView
								.findViewById(R.id.team_icon);
						Picasso.with(DetailsActivity.this)
								.load(Utils.getIconURL(d.getTeam()))
								.into(teamIcon);

						TextView s = (TextView) mDetailsDetailsView
								.findViewById(R.id.text_score);
						ImageView card = (ImageView) mDetailsDetailsView
								.findViewById(R.id.icon_card);

						if (d.getType().equals("0")) {
							s.setText(d.getTeam1Goals() + ":"
									+ d.getTeam2Goals());
						} else if (d.getType().equals("1")) {
							s.setVisibility(View.GONE);
							card.setVisibility(View.VISIBLE);
							card.setImageResource(R.drawable.card_yellow);
						} else if (d.getType().equals("2")) {
							s.setVisibility(View.GONE);
							card.setVisibility(View.VISIBLE);
							card.setImageResource(R.drawable.card_yellowred);
						} else if (d.getType().equals("3")) {
							s.setVisibility(View.GONE);
							card.setVisibility(View.VISIBLE);
							card.setImageResource(R.drawable.card_red);
						}

						i++;
						mMatches.addView(mDetailsDetailsView);
					}

					if (refresh) {
						Toast.makeText(getApplicationContext(),
								getString(R.string.dataloaded),
								Toast.LENGTH_SHORT).show();

						mRefresh.setRefreshing(false);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}
}
