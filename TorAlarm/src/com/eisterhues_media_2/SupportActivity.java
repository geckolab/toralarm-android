
package com.eisterhues_media_2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import butterknife.InjectView;
import butterknife.OnClick;

import com.eisterhues_media_2.activity.BaseActivity;
import com.eisterhues_media_2.application.TorApplication;
import com.eisterhues_media_2.network.BugReportNetworkHelper;
import com.eisterhues_media_2.network.BugReportNetworkHelper.BugNetworkHelperInterface;
import com.eisterhues_media_2.network.input.InputParams;
import com.eisterhues_media_2.utils.Const;
import com.eisterhues_media_2.utils.LogUtils;
import com.eisterhues_media_2.utils.NetworkUtils;
import com.eisterhues_media_2.utils.Utils;
import com.intentsoftware.addapptr.AATKit;

public class SupportActivity extends BaseActivity {
	private BugReportNetworkHelper mBugReportNetworkHelper;

	@InjectView(R.id.linearLayoutBackground)
	RelativeLayout mBackground;

	@InjectView(R.id.email)
	EditText mMail;

	@InjectView(R.id.msg)
	EditText mMsg;

	@OnClick(R.id.send)
	public void send(View view) {
		sendMessage();
	}

	@Override
	public int layout() {
		return R.layout.activity_support;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getTitleView().setText(getString(R.string.support));

		getBackButton().setVisibility(View.VISIBLE);
		getBackButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				finish();
			}
		});

		getParagraphButton().setVisibility(View.VISIBLE);

		Utils.loadBackground(getPrefsManager(), this, mBackground);

		mMail.setText(getPrefsManager().getUserID());

	}

	private void sendMessage() {
		if (!NetworkUtils.isNetworkAvailable(this))
			Toast.makeText(getApplicationContext(),
					getString(R.string.messagenotsent),
					Toast.LENGTH_SHORT).show();

		JSONObject settings = new JSONObject();

		if (!getPrefsManager().getPrefs().contains("1-push")) {
			try {
				LogUtils.d("MODE", "DEF");
				settings.put("0", new JSONArray(Const.PUSH_DE_DEFAULT));
			} catch (JSONException e) {
			}
		} else {
			String arr = "[";
			for (int i = 1; i <= Const.PUSH_DE_NUMBER; i++) {
				arr += getPrefsManager().getPush("1-" + i, 1);

				if (i < Const.PUSH_DE_NUMBER)
					arr += ",";
			}
			arr += "]";

			try {
				LogUtils.d("MODE", "USER");
				settings.put("0", new JSONArray(arr));
			} catch (JSONException e) {
			}
		}

		mBugReportNetworkHelper = new BugReportNetworkHelper();
		mBugReportNetworkHelper.sentApiRequest(this, InputParams.bugReport(getPrefsManager()
				.getDeviceID(),
				settings, Const.DEVICE_TYPE, getLanguage(), String.valueOf(getPrefsManager()
						.getAppVersion()), String.valueOf(android.os.Build.VERSION.SDK_INT),
				android.os.Build.MODEL,
				Utils.isTablet(this), getPrefsManager().getLastAction(), getPrefsManager()
						.getAppStarts(), mMail.getText().toString(), mMsg.getText().toString(), 1),
				new BugNetworkHelperInterface() {

					@Override
					public void apiResultAction() {
						Toast.makeText(getApplicationContext(),
								getString(R.string.messagesent),
								Toast.LENGTH_SHORT).show();
						finish();
					}
				});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			AATKit.onActivityResume(this);
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			Utils.addPlacementView(
					(FrameLayout) findViewById(R.id.frameLayoutAATKit),
					bannerPlacementId);
			AATKit.startPlacementAutoReload(bannerPlacementId);
		}
	}

	@Override
	protected void onPause() {
		if (Const.AATKIT && getPrefsManager().isAATKitEnable()) {
			int bannerPlacementId = ((TorApplication) getApplication())
					.getBannerPlacementId();
			AATKit.stopPlacementAutoReload(bannerPlacementId);
			Utils.removePlacementView(bannerPlacementId);
			AATKit.onActivityPause(this);
		}
		super.onPause();
	}

}
